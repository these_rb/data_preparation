"""
This module contains the functions used to make read/write
operation in the postgres DB.
"""

from math import ceil

import numpy as np


def unevenise(value):
    """
    Return an uneven value of even + 1
    """
    if value % 2 == 0:
        return value + 1
    return value


def init_coord_matrix(resolution_m, x_delt, y_delt, x_med, y_med):
    """
    Return a numpy array of a shape (x,y,3) corresponding to the bounding box
    of the building time the resolution_m in meters centred on the median x and y.
    The numpy array contains x and y coordinates in its (x,y,0:2) dimension.
    """
    x_dim = unevenise(int(ceil(x_delt) / resolution_m))
    y_dim = unevenise(int(ceil(y_delt) / resolution_m))

    rast_pts = np.zeros((x_dim, y_dim, 3))

    for xdi in range(x_dim):
        for ydi in range(y_dim):
            x_delta = xdi - (x_dim-1) / 2
            y_delta = ydi - (y_dim-1) / 2
            rast_pts[xdi, ydi, :] = [x_med + (x_delta*resolution_m), y_med + (y_delta*resolution_m), 0]

    return rast_pts


def pad_crop_matrix(matrix, x_dim=224, y_dim=224, crop=False):
    """
    Given a two dimensional matrix, return a matrix (x_dim, y_dim)
    padded around the original extent and/or cropped if the switch
    is activated.
    """
    if len(matrix.shape) > 2:
        raise Exception("Matrix should be two dimensional")

    # Both dimensions smaller or same size as determined
    if matrix.shape[0] <= x_dim and matrix.shape[1] <= y_dim:
        final_rast = np.zeros((x_dim, y_dim))
        # Non-empty raster to grow
        xstart = x_dim/2 - (matrix.shape[0]-1)/2
        ystart = y_dim/2 - (matrix.shape[1]-1)/2

        for x in range(matrix.shape[0]):
            for y in range(matrix.shape[1]):
                final_rast[int(xstart + x), int(ystart + y)] = matrix[x, y]
    # One dimension is smaller
    elif matrix.shape[0] <= x_dim or matrix.shape[1] <= y_dim:
        # Check that crop is activated
        if (matrix.shape[0] > x_dim or matrix.shape[1] > y_dim) and crop:
            if matrix.shape[0] > x_dim:
                # Crop the xdim and pad the ydim
                final_rast = np.zeros((matrix.shape[0], y_dim))
                xstart = matrix.shape[0]//2 - x_dim//2
                ystart = y_dim/2 - (matrix.shape[1]-1)/2

                for x in range(matrix.shape[0]):
                    for y in range(matrix.shape[1]):
                        final_rast[int(x), int(ystart + y)] = matrix[x, y]
                final_rast = final_rast[xstart:xstart+x_dim, :]

            else:
                # Pad the xdim and crop the ydim
                final_rast = np.zeros((x_dim, matrix.shape[1]))
                xstart = x_dim/2 - (matrix.shape[0]-1)/2
                ystart = matrix.shape[1]//2 - y_dim//2

                for x in range(matrix.shape[0]):
                    for y in range(matrix.shape[1]):
                        final_rast[int(xstart + x), int(y)] = matrix[x, y]
                final_rast = final_rast[:, ystart:ystart+y_dim]
        # If cropping is not wanted, abort
        else:
            raise Exception("Can't pad a matrix bigger than the final dimension without cropping")
    # Original matrix is bigger in both dimension, will be cropped
    elif crop:
        xstart = matrix.shape[0]//2 - x_dim//2
        ystart = matrix.shape[1]//2 - y_dim//2
        final_rast = matrix[xstart:xstart+x_dim, ystart:ystart+y_dim]
    # Original matrix is bigger in both dimension, abort
    else:
        raise Exception("Can't pad a matrix bigger than the final dimension without cropping")
    return final_rast


def init_raster_big(x_dim, y_dim, x_med, y_med, resolution_m):
    """"
    Returns a raster containing (x,y) coordinates separated by
    resolution_m  metres in the z dimension. The raster is padded
    with two extras rows/column in x and y dimension.
    Example given 128, 128 it will return a numpy array of
    shape (130,130,3) with last dimension being an array [x-coord, y-coord, 0]
    """
    x_dim_r = x_dim + 2
    y_dim_r = y_dim + 2
    rast_pts = np.zeros((x_dim_r, y_dim_r, 3))
    for x in range(0, x_dim_r):
        for y in range(0, y_dim_r):
            x_delta = x - (x_dim_r-1) / 2
            y_delta = y - (y_dim_r-1) / 2
            rast_pts[x, y, :] = [
                x_med + (x_delta * resolution_m), y_med + (y_delta * resolution_m), 0]
    return rast_pts
