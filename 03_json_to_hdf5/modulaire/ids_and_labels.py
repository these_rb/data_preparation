"""
This module contains the functions used to retrieve the labels
and create ids
"""
from modulaire.db_rw_operations import (add_roofs_in_db, get_real_surfaces,
                              remove_rows_from_db, write_surface_to_db)


def get_cat_and_class_from_building(building_json, cursor, connection,
                                delete_norslt_rows=True, delete_all_rows=False):
    """
    Given a building (or building part), cursor, connection this add the
    grounds geometries in the dedicated table and
    returns a dict of [cat, class] and [egid]. If any particularities
    arise, the returned dict is only made of the reason why
    the cat,class list couldn't be returned.
    In this case, the grounds geometries are removed from the
    dedicated table. Alternatively, if we are just checking the
    cat/class, all rows can be removed if the flag is set to true

    :param building_json: A cjio CityObject model representing a building
    :param cursor: A psycopg2 cursor
    :param connection: A psycopg2 connection
    :param delete_norslt_rows: Empty DB tables for building without cat
    :param delete_all_rows: Empty DB tables after execution
    """


    # If geometry is empty, return problem cause as string in array
    if not building_json.geometry:
        return {"catClass":['NoGeom']}

    # Retrieve indexes
    ground_idx = building_json.geometry[0].get_surfaces(type="GroundSurface")
    roof_idx = building_json.geometry[0].get_surfaces(type="RoofSurface")

    # Check if both roofs and grounds are present
    # else return problem cause as string in array
    if len(roof_idx) == 0 or len(ground_idx) == 0:
        return {"catClass":['NoRoofOrGround']}

    # Get surfaces coordinates from index
    ground_surfaces = get_real_surfaces(ground_idx, building_json.geometry[0])
    # Write to DB
    write_surface_to_db("grounds", ground_surfaces, cursor, connection)
    add_roofs_in_db(building_json, cursor, connection)

    # Select the regbl point(s) intersecting the building base
    geb_int = """SELECT DISTINCT gkat, gklas, egid
    FROM regbl_zone a, (SELECT geom, id FROM grounds UNION ALL SELECT geom, id FROM roofs) b
    WHERE ST_Intersects(a.geom, b.geom)"""

    # Check RegBl intersection
    cursor.execute(geb_int)

    # Check the status of the query result
    if cursor.statusmessage == 'SELECT 0':
        # No results returned (empty selection)
        if delete_norslt_rows or delete_all_rows:
            # Delete rows if wanted
            remove_rows_from_db(cursor, connection)
        # Return the cause as a string in array
        return {"catClass":['NoCat']}
    else:
        # Points intersecting the ground were found
        results = cursor.fetchall()

        if delete_all_rows:
            # Remove rows if wanted
            remove_rows_from_db(cursor, connection)

        # Check how many points were found
        if len(results) == 1:
            # Only one result
            # Verify that the cat and class are not NULL/None
            cat = results[0][0] if results[0][0] is not None else 'NoCat'
            klass = results[0][1] if results[0][1] is not None else 'NoClass'

            # Return an array of strings containing cat and class
            return {"catClass":[cat, klass], "egid":[results[0][2]]}

        # If several points were found
        else:
            # Retrieve results in separate lists
            cats_multi_rslt = []
            classes_multi_rslt = []
            egid_multi_rslt = []

            for rslt in results:
                cats_multi_rslt.append(rslt[0])
                classes_multi_rslt.append(rslt[1])
                egid_multi_rslt.append(rslt[2])

            egid = [eg for eg in set(egid_multi_rslt)]

            # If all results are of the same cat and same class
            if len(set(cats_multi_rslt)) == 1 and len(set(classes_multi_rslt)) == 1:
                # Verify that the cat and class are not NULL/None
                cat = cats_multi_rslt[0] if cats_multi_rslt[0] is not None else 'NoCat'
                klass = classes_multi_rslt[0] if classes_multi_rslt[0] is not None else 'NoClass'
                # Return an array of strings containing cat and class
                return {"catClass":[cat, klass], "egid":egid}

            # If only all categories are the same
            elif len(set(cats_multi_rslt)) == 1:
                cat = cats_multi_rslt[0] if cats_multi_rslt[0] is not None else 'NoCat'
                # Check if only two classes, included one None
                if len(set(classes_multi_rslt)) == 2 and None in set(classes_multi_rslt):
                    klass = classes_multi_rslt[0] if classes_multi_rslt[0] is not None else classes_multi_rslt[1]
                    return {"catClass":[cat, klass], "egid":egid}
                else:
                    return {"catClass":[cat, 'NoClass'], "egid":egid}

            # If only 2 cats, included one None
            elif len(set(cats_multi_rslt)) == 2 and None in set(cats_multi_rslt):
                cat = cats_multi_rslt[0] if cats_multi_rslt[0] is not None else cats_multi_rslt[1]
                # Check if only two classes, included one None
                if len(set(classes_multi_rslt)) == 2 and None in set(classes_multi_rslt):
                    klass = classes_multi_rslt[0] if classes_multi_rslt[0] is not None else classes_multi_rslt[1]
                    return {"catClass":[cat, klass], "egid":egid}
                else:
                    return {"catClass":[cat, 'NoClass'], "egid":egid}

            # Else multiple cats and classes
            else:
                if delete_norslt_rows:
                    # No results so delete rows from DB
                    remove_rows_from_db(cursor, connection)
                # Return the cause as a string in array
                return {"catClass":['MultipleCat']}
