"""
This module contains the functions used to make read/write
operation in the postgres DB.
"""
from psycopg2.extensions import (ISOLATION_LEVEL_AUTOCOMMIT,
                                 ISOLATION_LEVEL_DEFAULT)

from modulaire.raster_preparation import init_coord_matrix


def subset_table_from_cm_bbox(citymodel, cursor, connection, table_name):
    """
    Uses the citymodel to populate the zone table
    with a subset corresponding to its bbox + 1500 metres.
    Previous existing rows are first deleted.

    :param citymodel: A cityjson citymodel
    :param cursor: A psycopg2 cursor
    :param connection: A psycopg2 connection
    :param table_name: The name of the table to subset
    """
    if table_name not in ['geb', 'bdg', 'hway_poly']:
        raise ValueError(
            "Table name must be one of 'geb', 'bdg' or 'hway_poly'.")

    if table_name == 'geb':
        cursor.execute("DELETE FROM regbl_zone;")
        connection.commit()

        bbox_sql = """INSERT INTO regbl_zone
        SELECT egid, gkat, gklas, a.geom
        FROM geb a, (SELECT ST_MakeEnvelope(XMIN, YMIN, XMAX, YMAX, 2056) AS geom) b
        WHERE ST_DWithin(a.geom, b.geom, 1500)
        AND a.gstat = 1004
    AND a.gkat IS NOT NULL;"""

    elif table_name == 'bdg':
        cursor.execute("DELETE FROM bdg_zone;")
        connection.commit()

        bbox_sql = """INSERT INTO bdg_zone(geom)
        SELECT (ST_Dump(ST_Union(a.geom))).geom AS geom
        FROM bdg a, (SELECT ST_MakeEnvelope(XMIN, YMIN, XMAX, YMAX, 2056) AS geom) b
        WHERE ST_DWithin(a.geom, b.geom, 1500)"""
    else:
        cursor.execute("DELETE FROM hway_poly_zone;")
        connection.commit()

        bbox_sql = """INSERT INTO hway_poly_zone(geom)
        SELECT a.geom
        FROM hway_poly a, (SELECT ST_MakeEnvelope(XMIN, YMIN, XMAX, YMAX, 2056) AS geom) b
        WHERE ST_DWithin(a.geom, b.geom, 1500)"""

    bbox = citymodel.get_bbox()

    sql_q = bbox_sql
    sql_q = sql_q.replace("XMIN", str(bbox[0]))
    sql_q = sql_q.replace("YMIN", str(bbox[1]))
    sql_q = sql_q.replace("XMAX", str(bbox[3]))
    sql_q = sql_q.replace("YMAX", str(bbox[4]))
    cursor.execute(sql_q)
    connection.commit()

    # Vacuum analyze the table after the modification
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    if table_name == 'geb':
        cursor.execute("VACUUM ANALYZE regbl_zone")
    elif table_name == 'bdg':
        cursor.execute("VACUUM ANALYZE bdg_zone")
    else:
        cursor.execute("VACUUM ANALYZE hway_poly_zone")

    connection.set_isolation_level(ISOLATION_LEVEL_DEFAULT)


def remove_rows_from_db(cursor, connection, table_names=('grounds', 'roofs')):
    """
    Given a cursor and connection, remove all
    rows from the tables of the given list

    :param cursor: A psycopg2 cursor
    :param connection: A psycopg2 connection
    :param table_names: The name of the table to subset
    """
    sql = "DELETE FROM TABLE;"
    for i in table_names:
        sql_q = sql.replace("TABLE", i)
        cursor.execute(sql_q)
    connection.commit()


def get_real_surfaces(surf_idx, geometry):
    """
    Return a nested list of coordinates corresponding to the vertexes of
    the triangles reprensenting the surfaces of the building.

    :param surf_idx: The result of .get_surfaces(type="...") for a given type
    :param geometry: A cjio geometry model
    """
    surfaces = []
    for group in surf_idx:
        indexes = surf_idx[group]['surface_idx']
        for idx in indexes:
            surfaces.append(geometry.get_surfaces()[idx[0]])
    return surfaces


def write_surface_to_db(table_name, surfaces, cursor, connection):
    """
    Write the given surfaces to the specified table in postgres

    :param table_name: Name of the table where to write surfaces
    :param surfaces: Nested list of coordinates obtained by get_real_surfaces()
    :param cursor: A psycopg2 cursor
    :param connection: A psycopg2 connection
    """
    insert_sql = """INSERT INTO TABLE(geom)
    VALUES (ST_GeomFromText('POLYGONZ((REPL0, REPL1, REPL2, REPL0))', 2056));"""
    for surf in surfaces:
        sql_q = insert_sql
        for coord in range(0, 3):  # Always triangles
            coords_text = " ".join(str(e) for e in surf[0][coord])
            str_to_replace = "REPL"+str(coord)
            sql_q = sql_q.replace(str_to_replace, coords_text)
            sql_q = sql_q.replace("TABLE", table_name)
        cursor.execute(sql_q)
    connection.commit()


def add_roofs_in_db(building_json, cursor, connection):
    """
    Write the roof surfaces of the given building to the dedicated table ("roofs")

    :param building_json: A cjio CityObject model representing a building
    :param cursor: A psycopg2 cursor
    :param connection: A psycopg2 connection
    """
    roof_idx = building_json.geometry[0].get_surfaces(type="RoofSurface")
    if len(roof_idx) > 0:
        roof_surfaces = get_real_surfaces(roof_idx, building_json.geometry[0])
        write_surface_to_db("roofs", roof_surfaces, cursor, connection)


def bbox_helpers_db(cursor):
    """
    Return the difference between max and min
    and the median value for x and y based
    on the surfaces (grounds and roofs) in the database

    :param cursor: A psycopg2 cursor
    """
    sql = """WITH tot AS (
        SELECT geom, id FROM grounds
        UNION ALL
        SELECT geom, id FROM roofs
    ), box AS (
        SELECT ST_Extent(geom) AS bbox
        FROM tot
    )
    SELECT ST_XMin(bbox) AS xmin, ST_XMax(bbox) AS xmax, ST_YMin(bbox) AS ymin, ST_YMax(bbox) AS ymax
    FROM box;"""
    cursor.execute(sql)
    xmin, xmax, ymin, ymax = cursor.fetchone()

    x_diff = xmax - xmin
    x_med = xmin + x_diff/2

    y_diff = ymax - ymin
    y_med = ymin + y_diff/2

    return (x_diff, x_med, y_diff, y_med)


def retrieve_heights(coord_matrix, cursor):
    """
    Return a numpy array containing the height
    difference between roof and ground for
    every pixel in the input raster

    :param coord_matrix: A numpy array of shape (xdim, ydim, (x_coord, y_coord, 0))
    :param cursor: A psycopg2 cursor
    """
    height_bsql = """
    WITH line AS (
        SELECT ST_GeomFromText('LINESTRING Z(REPL 190, REPL 4640)', 2056) AS geom
    ), int_gnd AS (
        SELECT DISTINCT CASE
                            WHEN ST_AsText(ST_3dIntersection(a.geom, b.geom)) = text('GEOMETRYCOLLECTION EMPTY') THEN ST_3DClosestPoint(a.geom, b.geom)
                            ELSE ST_3dIntersection(a.geom, b.geom)
                        END AS geom
        FROM line a, grounds b
        WHERE ST_3DIntersects(a.geom, b.geom)
    ), int_roof AS (
        SELECT DISTINCT CASE
                            WHEN ST_AsText(ST_3dIntersection(a.geom, b.geom)) = text('GEOMETRYCOLLECTION EMPTY') THEN ST_3DClosestPoint(a.geom, b.geom)
                            ELSE ST_3dIntersection(a.geom, b.geom)
                        END AS geom
        FROM line a, roofs b
        WHERE ST_3DIntersects(a.geom, b.geom)
    ), alt_gnd AS (
        SELECT CASE
                    WHEN tot_int_g > 1 THEN sumZ / tot_int_g
                    ELSE sumZ
               END as alti
        FROM (SELECT DISTINCT COUNT(*) AS tot_int_g, SUM(ST_Z(geom)) AS sumZ FROM int_gnd) tig, int_gnd b
    ), alt_roof AS (
        SELECT CASE
                    WHEN tot_int_r > 1 THEN sumZ / tot_int_r
                    ELSE sumZ
               END as alti
        FROM (SELECT DISTINCT COUNT(*) AS tot_int_r, SUM(ST_Z(geom)) AS sumZ FROM int_roof) tig, int_roof b
    )
    SELECT r.alti - g.alti AS height
    FROM alt_roof r, alt_gnd g;"""

    for xdi in range(coord_matrix.shape[0]):
        for ydi in range(coord_matrix.shape[1]):
            sql_q = height_bsql
            coords_text = " ".join(str(e) for e in coord_matrix[xdi, ydi, 0:2])
            sql_q = sql_q.replace("REPL", coords_text)
            cursor.execute(sql_q)
            if cursor.statusmessage == 'SELECT 0':
                coord_matrix[xdi, ydi, 2] = 0
            else:
                coord_matrix[xdi, ydi, 2] = round(cursor.fetchone()[0], 3)
    return coord_matrix


def retrieve_rast_presence(resolution_m, x_med, y_med, cursor, table_name, x_dim=224, y_dim=224):
    """
    Return a numpy array containing the built percentage
    for every pixel of a raster based on the input dimensions
    and resolution in meters. The resolution also determines the
    size of the surroundings taken, for example, a resolution of
    1 meter takes x_dim, y_dim meters around the center and a resolution
    of 2 meters takes 2*x_dim and 2*y_dim around the center but gives
    an array of size (x_dim, y_dim).
    """
    rast_query = """WITH pix AS (
                    SELECT ST_MakeEnvelope(x_min, y_min, x_max, y_max, 2056) AS geom
                ), intersection AS (
                    SELECT (ST_Dump(ST_Union(ST_Intersection(a.geom, b.geom)))).geom
                    FROM pix a, TABLENAME b
                    WHERE ST_Intersects(a.geom, b.geom)
                ), area AS (
                    SELECT ROUND((ST_Area(geom) / SQUAREAREA)::numeric, 4) AS aire
                    FROM intersection
                )
                SELECT * FROM area"""
    rast_query = rast_query.replace("TABLENAME", str(table_name))
    rast_query = rast_query.replace(
        "SQUAREAREA", str(resolution_m*resolution_m))

    # Create a raster of previously defined size and resolution
    raster = init_coord_matrix(resolution_m, x_dim*resolution_m, y_dim*resolution_m, x_med, y_med)

    for i in range(1, raster.shape[0]-1):
        for j in range(1, raster.shape[0]-1):

            xmin, ymin = raster[i-1, j-1, :2]
            xmax, ymax = raster[i+1, j+1, :2]

            sql_q = rast_query
            sql_q = sql_q.replace("x_min", str(xmin + (resolution_m/2)))
            sql_q = sql_q.replace("y_min", str(ymin + (resolution_m/2)))
            sql_q = sql_q.replace("x_max", str(xmax - (resolution_m/2)))
            sql_q = sql_q.replace("y_max", str(ymax - (resolution_m/2)))

            cursor.execute(sql_q)
            if cursor.statusmessage != 'SELECT 0':
                raster[i, j, 2] = cursor.fetchone()[0]

    raster = raster[1:x_dim+1, 1:y_dim+1, 2]
    return raster
