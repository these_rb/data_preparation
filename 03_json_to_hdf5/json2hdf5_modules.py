"""
Refonte du code de création des hdf5 avec utilisation
des modules créés. Pour l'instant, les opération sont testées
au niveau de simple buildings (lignes 400 env.)
Le readme liste la suite des choses à corriger.
"""

import argparse
import os
from datetime import datetime

import numpy as np
import psycopg2 as db
from cjio import cityjson

from modulaire.db_rw_operations import (bbox_helpers_db, remove_rows_from_db,
                                        retrieve_heights,
                                        subset_table_from_cm_bbox)
from modulaire.ids_and_labels import get_cat_and_class_from_building
from modulaire.raster_preparation import init_coord_matrix, pad_crop_matrix

# Argparsing
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input_folder_path", required=True,
                help="Input folder path string")
ap.add_argument("-d", "--db_name", required=True, help="Regbl database name")
ap.add_argument("-u", "--db_username", required=True, help="database username")
ap.add_argument("-p", "--db_password", help="database password")
ap.add_argument("-s", "--filename_suffix", required=True,
                help="Suffix to append to output filename (e.g. _1)")
ap.add_argument("-o", "--output_folder_path", required=True,
                help="Output folder path string")
args = ap.parse_args()


# Retrieve the json path
json_files_to_convert = []
for fname in os.listdir(args.input_folder_path):
    json_files_to_convert.append(os.path.join(args.input_folder_path, fname))
json_files_to_convert = sorted(json_files_to_convert)

# print(*json_files_to_convert, sep='\n')

# # Create two hdf5 files
# cat_file = h5py.File(os.path.join(args.output_folder_path,
#                                   'categories' + args.filename_suffix + '.hdf5'), 'w')
# class_file = h5py.File(os.path.join(
#     args.output_folder_path, 'classes' + args.filename_suffix + '.hdf5'), 'w')

# cat_file.create_dataset("rasters_cat", shape=(
#     1, 128, 128, 3), maxshape=(None, 128, 128, 3), dtype="float")
# cat_file.create_dataset("labels_cat", shape=(
#     1, ), maxshape=(None, ), dtype="int")

# class_file.create_dataset("rasters_class", shape=(
#     1, 128, 128, 3), maxshape=(None, 128, 128, 3), dtype="float")
# class_file.create_dataset("labels_class", shape=(1, ),
#                           maxshape=(None, ), dtype="int")

# cat_file.close()
# class_file.close()

# Iterate on each json file
for path in json_files_to_convert:
    # Print file name and starting time
    print(f"Starting file {os.path.split(path)[-1]} - {datetime.now()}")

    # Initialise the variables
    results_rast_cat, results_rast_class = {}, {}
    final_result_cat, final_result_class = {}, {}
    possible_cats = ['1020', '1030', '1040', '1060', '1080']
    possible_classes = ['1110', '1121', '1122', '1130', '1211', '1212', '1220',
                        '1230', '1231', '1241', '1242', '1251', '1252', '1261',
                        '1262', '1263', '1264', '1265', '1271', '1272', '1273', '1274']
    for i in possible_cats:
        results_rast_cat[i] = []
        final_result_cat[i] = None

    for i in possible_classes:
        results_rast_class[i] = []
        final_result_class[i] = None

    # Retrieve the citymodel
    cm = cityjson.load(path)
    buildings = cm.get_cityobjects(type=["Building", "BuildingPart"])
    # Keep a set of building installation
    building_installation_list = set(
        cm.get_cityobjects(type=["BuildingInstallation"]).keys())
    # DB connection
    connection_string = "dbname='" + args.db_name + "' user='" + args.db_username + "' host='localhost' password='" + args.db_password + \
        "'" if args.db_password is not None else "dbname='" + args.db_name + \
        "' user='" + args.db_username + "' host='localhost'"
    conn = db.connect(connection_string)
    cur = conn.cursor()

    # Subset geb, bdg and roads
    subset_table_from_cm_bbox(cm, cur, conn, 'geb')
    subset_table_from_cm_bbox(cm, cur, conn, 'bdg')
    subset_table_from_cm_bbox(cm, cur, conn, 'hway_poly')

    # Make sure roof and ground tables are clean
    remove_rows_from_db(cur, conn)

    for bd_id in buildings:
        # Identify and retrieve building
        # print(i)
        b = buildings[bd_id]

        # Caracterise building
        has_child = True if len(b.children) > 0 else False
        has_parent = True if len(b.parents) > 0 else False
        has_geom = True if len(b.geometry) > 0 else False
        valid = True if has_geom or (not has_geom and has_child) else False

        # Make sure previous buildings geometries are removed
        remove_rows_from_db(cur, conn)

        if not valid:
            # Building can't be used
            # stats_non_valid+=1
            # print("Aborting, cause invalid")
            # print("")
            continue
        elif has_parent:
            # Children are analyzed at their parent level
            # stats_children+=1
            # print("Aborting, cause done at the parent level")
            continue
        elif has_child:
            # Children need to be analyzed first
            # stats_parent+=1
            # print("Need further computing")
            # CHILDREN
            #### Children evaluation part ####
            children_cats = []
            children_classes = []
            egids_children = []

            children = list(b.children)
            # Remove ids of building installation
            for child in list(children):
                if child in building_installation_list:
                    children.remove(child)
                    # print("install removed")

            # Retrieve class and category of the children
            for child in list(children):
                cb = buildings[child]
                # check if child has a geometry
                if len(cb.geometry) == 0:
                    # If not, don't consider this child anymore
                    children.remove(child)
                    continue
                # Retrieve the cat and class
                rslts = get_cat_and_class_from_building(
                    cb, cur, conn, delete_all_rows=True)

                # If problem, remove child
                if len(rslts['catClass']) < 2 and rslts['catClass'][0] in ['NoGeom', 'NoRoofOrGround']:
                    children.remove(child)
                    continue
                # elif Nocat assign to cat and class
                elif len(rslts['catClass']) < 2:
                    children_cats.append(rslts['catClass'][0])
                    children_classes.append(rslts['catClass'][0])
                # else assign to their respective list
                else:
                    children_cats.append(rslts['catClass'][0])
                    children_classes.append(rslts['catClass'][1])
                    egids_children.append(rslts['egid'])

            # Retrieve sets of unique elements
            cats_without_NoCat = list(set(children_cats))
            if 'NoCat' in children_cats:
                cats_without_NoCat.remove('NoCat')
            if 'MultipleCat' in children_cats:
                cats_without_NoCat.remove('MultipleCat')

            class_wo_NoClass = list(set(children_classes))
            if 'NoCat' in children_classes:
                class_wo_NoClass.remove('NoCat')
            if 'NoClass' in children_classes:
                class_wo_NoClass.remove('NoClass')
            if 'MultipleCat' in children_classes:
                class_wo_NoClass.remove('MultipleCat')

            #### Children assignment part ####

            # No more children geometries

            if len(children) == 0 and not has_geom:
                # print("Aborting, cause no child and no geom")
                continue
            # No children and a geom -> simple building
            elif len(children) == 0 and has_geom:
                # simple building
                rslts = get_cat_and_class_from_building(b, cur, conn)
                if len(rslts['catClass']) < 2:
                    # print("Aborting, cause {}".format(catNclass[0]))
                    continue
                else:
                    # Tout recuperer
                    # print("No child but geom", rslts)
                    #### TODO LA SUITE COMME BDG
                    continue

            # Same children's category
            if len(set(children_cats)) == 1 or len(cats_without_NoCat) == 1:

                # Only category is None
                if len(cats_without_NoCat) == 0:
                    if not has_geom:
                        # if children don't have a cat and parent hasn't too
                        # print("Aborting, cause parent and children have NoCat")
                        continue
                    else:
                        # Else use cat of parent for all parts
                        rslts = get_cat_and_class_from_building(b, cur, conn)
                        if len(rslts['catClass']) < 2:
                            # print("Aborting, cause parent has {} and children have NoCat".format(catNclass[0]))
                            continue
                        else:
                            # Add geometries of children to db tables
                            for i, chd in enumerate(children):
                                if children_cats[i] not in ['NoGeom', 'NoRoofOrGround']:
                                    cb = buildings[chd]
                                    get_cat_and_class_from_building(
                                        cb, cur, conn, delete_norslt_rows=False)
                            # Get the first of all children_egids
                            egid = [egid if len(egid) < 2 else egid[0] for egid in egids_children][0]
                            #### TODO LA SUITE COMME BDG
                            # Add to results for cat
                            # results_rast_cat[str(catNclass[0])].append(final_rast)
                            # Add result for class
                            #if catNclass[1] != 'NoClass':
                                #results_rast_class[str(catNclass[1])].append(final_rast)
                            continue

                # Only one numerical category
                else:
                    if not has_geom:
                        # If building hasn't any geom use the children cat
                        cat = cats_without_NoCat[0]
                    else:
                        # Check if children and parent have same cat
                        rslts = get_cat_and_class_from_building(b, cur, conn)
                        # If parent has no geom continue to children
                        if len(rslts['catClass']) < 2:
                            if rslts['catClass'][0] in ['NoGeom', 'NoRoofOrGround', 'MultipleCat']:
                                remove_rows_from_db(cur, conn)
                        # If p and c do not share the same cat, add a raster for parent
                        elif rslts['catClass'][0] != list(set(children_cats))[0]:
                            # rslts['egid']
                            # Add to results for cat
                            # results_rast_cat[str(catNclass[0])].append(final_rast)
                            # Add result for class
                            #if catNclass[1] != 'NoClass':
                            #    results_rast_class[str(catNclass[1])].append(final_rast)

                            # Remove building geometries to make way for children geoms
                            remove_rows_from_db(cur, conn)

                    # Add geometries of children to db tables
                    for i, chd in enumerate(children):
                        if children_cats[i] not in ['NoGeom', 'NoRoofOrGround']:
                            cb = buildings[chd]
                            get_cat_and_class_from_building(
                                cb, cur, conn, delete_norslt_rows=False)
                    # Get the first of all children_egids
                    egid = [egid if len(egid) < 2 else egid[0] for egid in egids_children][0]
                    #### TODO LA SUITE COMME BDG

                    # Add to results for cat
                    #results_rast_cat[str(cat)].append(final_rast)
                    # Add to results for class
                    #if class_wo_NoClass == 1:
                        #results_rast_class[str(class_wo_NoClass[0])].append(final_rast)
                        #continue

            # Multiple categories
            else:
                child_diff_cat = {}
                for cat in cats_without_NoCat:
                    idx = [i for i, x in enumerate(children_cats) if x == cat]
                    child_diff_cat[cat] = idx

                # No parent geometry
                if not has_geom:
                    # Add all buildings (exception de NoCat)
                    for cat in cats_without_NoCat:
                        egids_children_cat = []
                        for bat in child_diff_cat[cat]:
                            cb = buildings[children[bat]]
                            rslts = get_cat_and_class_from_building(
                                cb, cur, conn, delete_norslt_rows=False)
                            if len(rslts['catClass']) > 1:
                                egids_children_cat.append(rslts['egid'])
                        # Get the first of all children_egids
                        egid = [egid if len(egid) < 2 else egid[0] for egid in egids_children_cat][0]
                        #### TODO LA SUITE COMME BDG
                        # Add to results for cat
                        #results_rast_cat[str(egid)].append(final_rast)

                        # Remove geometries before continuing
                        remove_rows_from_db(cur, conn)


                # Parent has a geometry
                else:
                    rslts_parent = get_cat_and_class_from_building(
                        b, cur, conn, delete_all_rows=True)
                    # If parent has cat n class
                    if len(rslts_parent['catClass']) > 1:
                        # Create a raster for each category
                        for cat in cats_without_NoCat:
                            egids_children_cat = []
                            # Add all children geom corresponding to cat
                            for bat in child_diff_cat[cat]:
                                cb = buildings[children[bat]]
                                rslts = get_cat_and_class_from_building(
                                    cb, cur, conn, delete_norslt_rows=False)
                                if len(rslts['catClass']) > 1:
                                    egids_children_cat.append(rslts['egid'])
                            # Get the first of all children_egids
                            egid = [egid if len(egid) < 2 else egid[0] for egid in egids_children_cat][0]
                            # If cat of children is the same as parent, add p geom
                            if cat == rslts_parent['catClass'][0]:
                                # Add geometries to db table
                                get_cat_and_class_from_building(
                                    b, cur, conn, delete_norslt_rows=False)
                                egid = rslts_parent['egid']
                            #### TODO LA SUITE COMME BDG
                            # Add to results for cat
                            # results_rast_cat[str(egid)].append(final_rast)

                            # Remove geometries before continuing
                            remove_rows_from_db(cur, conn)

            # Multiple class for children
            if len(class_wo_NoClass) > 1:

                child_diff_class = {}
                for klass in class_wo_NoClass:
                    idx = [i for i, x in enumerate(children_classes) if x == klass]
                    child_diff_class[klass] = idx

                # No parent geometry
                if not has_geom:
                    # Add buildings for each class (exception de NoClass)
                    for klass in class_wo_NoClass:
                        egids_children_class = []
                        for bat in child_diff_class[klass]:
                            cb = buildings[children[bat]]
                            rslts = get_cat_and_class_from_building(
                                cb, cur, conn, delete_norslt_rows=False)
                            if len(rslts['catClass']) > 1:
                                egids_children_class.append(rslts['egid'])
                        # Get the first of all children_egids
                        egid = [egid if len(egid) < 2 else egid[0] for egid in egids_children_class][0]
                        # Add to results for class
                        #results_rast_class[str(klass)].append(final_rast)

                        # Remove geometries before continuing
                        remove_rows_from_db(cur, conn)

                # Parent geometry
                else:
                    rslts_parent = get_cat_and_class_from_building(
                        b, cur, conn, delete_all_rows=True)
                    # If parent has cat n class
                    if len(rslts_parent['catClass']) > 1:
                        # Create a raster for each category
                        for klass in class_wo_NoClass:
                            egids_children_class = []
                            # Add all children geom corresponding to cat
                            for bat in child_diff_class[klass]:
                                cb = buildings[children[bat]]
                                rslts = get_cat_and_class_from_building(
                                    cb, cur, conn, delete_norslt_rows=False)
                                if len(rslts['catClass']) > 1:
                                    egids_children_cat.append(rslts['egid'])
                            # Get the first of all children_egids
                            egid = [egid if len(egid) < 2 else egid[0] for egid in egids_children_cat][0]
                            # If cat of children is the same as parent, add p geom
                            if klass == rslts_parent['catClass'][1]:
                                # Add geometries to db table
                                get_cat_and_class_from_building(
                                    b, cur, conn, delete_norslt_rows=False)
                                egid = rslts_parent['egid']

                            # TODO: La suite
                            # Add to results for cat
                            # results_rast_class[str(klass)].append(final_rast)

                            # Remove geometries before continuing
                            remove_rows_from_db(cur, conn)
            # End of children
            continue
        else:
        # SIMPLE BUILDING
            # Simple building (no parent, no child, a geom)
            # stats_simple+=1
            # print("Single building")

            # Retrieve cat and class of building
            rslts = get_cat_and_class_from_building(b, cur, conn)

            # Check if failure cause or results were returned
            if len(rslts['catClass']) < 2:
                # print(f"Aborting, cause {rslts['catClass'][0]}")
                # stat_simple_fail+=1
                # failed.append(i)
                # print("")
                # If no cat/class, go to next building
                continue
            # If results were returned
            else:
                # print(rslts)
                # Compute the coordinates
                x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                # # Initialise the building's coordinates raster
                rast_bd = init_coord_matrix(1, x_diff, y_diff, x_med, y_med)
                # Retrieve heights
                rast_height = retrieve_heights(rast_bd, cur)
                # If there are any heights
                if np.any(np.unique(rast_height[:, :, 2])) != 0:
                    # Standardise size of array of heights
                    height_mat = pad_crop_matrix(rast_height[:,:,2], 224, 224, crop=True)
		    # Get the roand and building coverage
		    bdg_around = retrieve_rast_presence(1, 224, 224, x_med, y_med, cur, "bdg_zone")
		    road_around = retrieve_rast_presence(1, 224, 224, x_med, y_med, cur, "hway_poly_zone")
		    
                #         # Add to results for cat
                #         results_rast_cat[str(catNclass[0])].append(final_rast)
                #         # Add result for class
                #         if catNclass[1] != 'NoClass':
                #             results_rast_class[str(catNclass[1])].append(
                #                 final_rast)

                # Erase table for next iter
                remove_rows_from_db(cur, conn)

    cur.close()
    conn.close()

### REST OF THE PRO JSON FILE ELEMENTS
