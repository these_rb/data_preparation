"""
CECI EST LE CODE ORIGINAL AVEC LA DÉFINITION DES FONCTIONS
AINSI QUE LA CRÉATION DES HDF5. Il faut regarder dans l'autre fichier .py
"""


import argparse
import os
from datetime import datetime
from math import ceil

import h5py
import numpy as np
import psycopg2 as db
from cjio import cityjson
from psycopg2.extensions import (ISOLATION_LEVEL_AUTOCOMMIT,
                                 ISOLATION_LEVEL_DEFAULT)

# Argparsing
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input_folder_path", required=True,
                help="Input folder path string")
ap.add_argument("-d", "--db_name", required=True, help="Regbl database name")
ap.add_argument("-u", "--db_username", required=True, help="database username")
ap.add_argument("-p", "--db_password", help="database password")
ap.add_argument("-s", "--filename_suffix", required=True,
                help="Suffix to append to output filename (e.g. _1)")
ap.add_argument("-o", "--output_folder_path", required=True,
                help="Output folder path string")
args = ap.parse_args()


# Definitions part
def subset_table_from_cm_bbox(citymodel, cur, conn, dbtable):
    """
    Subset the given db table from the cityjson bbox
    into another given_zone table
    Previous existing rows are first deleted
    """
    if dbtable not in ['geb', 'bdg', 'hway_poly']:
        raise ValueError(
            "Table name must be one of 'geb', 'bdg' or 'hway_poly'.")

    if dbtable == 'geb':
        cur.execute("DELETE FROM regbl_zone;")
        conn.commit()

        bbox_sql = """INSERT INTO regbl_zone
        SELECT egid, gkat, gklas, a.geom
        FROM geb a, (SELECT ST_MakeEnvelope(XMIN, YMIN, XMAX, YMAX, 2056) AS geom) b
        WHERE ST_DWithin(a.geom, b.geom, 500)
        AND a.gstat = 1004
    AND a.gkat IS NOT NULL;"""

    elif dbtable == 'bdg':
        cur.execute("DELETE FROM bdg_zone;")
        conn.commit()

        bbox_sql = """INSERT INTO bdg_zone(geom)
        SELECT (ST_Dump(ST_Union(a.geom))).geom AS geom
        FROM bdg a, (SELECT ST_MakeEnvelope(XMIN, YMIN, XMAX, YMAX, 2056) AS geom) b
        WHERE ST_DWithin(a.geom, b.geom, 500)"""
    else:
        cur.execute("DELETE FROM hway_poly_zone;")
        conn.commit()

        bbox_sql = """INSERT INTO hway_poly_zone(geom)
        SELECT a.geom
        FROM hway_poly a, (SELECT ST_MakeEnvelope(XMIN, YMIN, XMAX, YMAX, 2056) AS geom) b
        WHERE ST_DWithin(a.geom, b.geom, 500)"""

    bbox = citymodel.get_bbox()

    sql_q = bbox_sql
    sql_q = sql_q.replace("XMIN", str(bbox[0]))
    sql_q = sql_q.replace("YMIN", str(bbox[1]))
    sql_q = sql_q.replace("XMAX", str(bbox[3]))
    sql_q = sql_q.replace("YMAX", str(bbox[4]))
    cur.execute(sql_q)
    conn.commit()

    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    if dbtable == 'geb':
        cur.execute("VACUUM ANALYZE regbl_zone")
    elif dbtable == 'bdg':
        cur.execute("VACUUM ANALYZE bdg_zone")
    else:
        cur.execute("VACUUM ANALYZE hway_poly_zone")

    conn.set_isolation_level(ISOLATION_LEVEL_DEFAULT)


def remove_rows_from_db(cursor, connection, table_names=('grounds', 'roofs')):
    """
    Given a cursor and connection, remove all
    rows from the table of the given list
    """
    sql = "DELETE FROM TABLE;"
    for i in table_names:
        sql_q = sql.replace("TABLE", i)
        cursor.execute(sql_q)
    connection.commit()


def write_surface_to_db(table_name, surfaces, cursor, connection):
    """
    Given a query this writes the surfaces to
    the specified table in postgres
    """
    insert_sql = """INSERT INTO TABLE(geom)
    VALUES (ST_GeomFromText('POLYGONZ((REPL0, REPL1, REPL2, REPL0))', 2056));"""
    for surf in surfaces:
        sql_q = insert_sql
        for coord in range(0, 3):  # Always triangles
            coords_text = " ".join(str(e) for e in surf[0][coord])
            str_to_replace = "REPL"+str(coord)
            sql_q = sql_q.replace(str_to_replace, coords_text)
            sql_q = sql_q.replace("TABLE", table_name)
        cursor.execute(sql_q)
    connection.commit()


def add_roofs_in_db(building_json, cursor, connection):
    """
    Given a building in json and a query, adds the
    roof surfaces in the database
    """
    roof_idx = building_json.geometry[0].get_surfaces(type="RoofSurface")
    if len(roof_idx) > 0:
        roof_surfaces = []
        get_real_surfaces(roof_surfaces, roof_idx, building_json.geometry[0])
        write_surface_to_db("roofs", roof_surfaces, cursor, connection)


def get_real_surfaces(list_name, idx, geometry):
    """
    Add surfaces (triangles) of geometry
    to list given the index of surfaces and
    the geometry of the building
    """
    for r in idx:
        vals = idx[r]['surface_idx']
        for v in vals:
            list_name.append(geometry.get_surfaces()[v[0]])


def get_cat_and_class_from_building(building_json, cursor, connection,
                                delete_norslt_rows=True, delete_all_rows=False):
    """
    Given a building, cursor, connection this add the
    grounds geometries in the dedicated table and
    returns a list of [cat, class]. If any particularities
    arise, the returned list is only made of the reason why
    the cat,class list couldn't be returned.
    In this case, the grounds geometries are removed from the
    dedicated table. Alternatively, if we are just checking the
    cat/class, all rows can be removed if the flag is set to true
    """


    # If geometry is empty, return problem cause
    # as string in array
    if not len(building_json.geometry):
        return ['NoGeom']

    # Retrieve indexes
    ground_idx = building_json.geometry[0].get_surfaces(type="GroundSurface")
    roof_idx = building_json.geometry[0].get_surfaces(type="RoofSurface")

    # Check if both roofs and grounds are present
    # else return problem cause as string in array
    if len(roof_idx) == 0 or len(ground_idx) == 0:
        return ['NoRoofOrGround']

    # Get surfaces coordinates from index
    ground_surfaces = []
    get_real_surfaces(ground_surfaces, ground_idx, building_json.geometry[0])
    # Write to DB
    write_surface_to_db("grounds", ground_surfaces, cursor, connection)
    add_roofs_in_db(building_json, cursor, connection)

    # Select the regbl point(s) intersecting the building base
    geb_int = """SELECT DISTINCT gkat, gklas
    FROM regbl_zone a, (SELECT geom, id FROM grounds UNION ALL SELECT geom, id FROM roofs) b
    WHERE ST_Intersects(a.geom, b.geom)"""

    # Check RegBl intersection
    cursor.execute(geb_int)

    # Check the status of the query result
    if cursor.statusmessage == 'SELECT 0':
        # No results returned (empty selection)
        if delete_norslt_rows or delete_all_rows:
            # Delete rows if wanted
            remove_rows_from_db(cursor, connection)
        # Return the cause as a string in array
        return ['NoCat']
    else:
        # Points intersecting the ground were found
        results = cursor.fetchall()

        if delete_all_rows:
            # Remove rows if wanted
            remove_rows_from_db(cursor, connection)

        # Check how many points were found
        if len(results) == 1:
            # Only one result
            # Verify that the cat and class are not NULL/None
            cat = results[0][0] if results[0][0] is not None else 'NoCat'
            klass = results[0][1] if results[0][1] is not None else 'NoClass'

            # Return an array of strings containing cat and class
            return [cat, klass]

        # If several points were found
        else:
            # Retrieve results in separate lists
            cats_multi_rslt = []
            classes_multi_rslt = []

            for rslt in results:
                cats_multi_rslt.append(rslt[0])
                classes_multi_rslt.append(rslt[1])

            # If all results are of the same cat and same class
            if len(set(cats_multi_rslt)) == 1 and len(set(classes_multi_rslt)) == 1:
                # Verify that the cat and class are not NULL/None
                cat = cats_multi_rslt[0] if cats_multi_rslt[0] is not None else 'NoCat'
                klass = classes_multi_rslt[0] if classes_multi_rslt[0] is not None else 'NoClass'
                # Return an array of strings containing cat and class
                return [cat, klass]

            # If only all categories are the same
            elif len(set(cats_multi_rslt)) == 1:
                cat = cats_multi_rslt[0] if cats_multi_rslt[0] is not None else 'NoCat'
                # Check if only two classes, included one None
                if len(set(classes_multi_rslt)) == 2 and None in set(classes_multi_rslt):
                    klass = classes_multi_rslt[0] if classes_multi_rslt[0] is not None else classes_multi_rslt[1]
                    return [cat, klass]
                else:
                    return [cat, 'NoClass']

            # If only 2 cats, included one None
            elif len(set(cats_multi_rslt)) == 2 and None in set(cats_multi_rslt):
                cat = cats_multi_rslt[0] if cats_multi_rslt[0] is not None else cats_multi_rslt[1]
                # Check if only two classes, included one None
                if len(set(classes_multi_rslt)) == 2 and None in set(classes_multi_rslt):
                    klass = classes_multi_rslt[0] if classes_multi_rslt[0] is not None else classes_multi_rslt[1]
                    return [cat, klass]
                else:
                    return [cat, 'NoClass']

            # Else multiple cats and classes
            else:
                if delete_norslt_rows:
                    # No results so delete rows from DB
                    remove_rows_from_db(cursor, connection)
                # Return the cause as a string in array
                return ['MultipleCat']


def bbox_helpers_db(cursor):
    """
    Return the difference between max and min
    and the median value for x and y based
    on the surfaces in the database
    """
    sql = """WITH tot AS (
        SELECT geom, id FROM grounds
        UNION ALL
        SELECT geom, id FROM roofs
    ), box AS (
        SELECT ST_Extent(geom) AS bbox
        FROM tot
    )
    SELECT ST_XMin(bbox) AS xmin, ST_XMax(bbox) AS xmax, ST_YMin(bbox) AS ymin, ST_YMax(bbox) AS ymax
    FROM box;"""
    cursor.execute(sql)
    xmin, xmax, ymin, ymax = cursor.fetchone()

    x_diff = xmax - xmin
    x_med = xmin + x_diff/2

    y_diff = ymax - ymin
    y_med = ymin + y_diff/2

    return (x_diff, x_med, y_diff, y_med)


def unevenise(value):
    """
    Return an uneven value
    of even + 1
    """
    if value % 2 == 0:
        return value + 1
    else:
        return value


def init_rast(resolution_m, x_delt, y_delt, x_med, y_med):
    """
    Return a numpy array of resolution m in meters
    centred on the median x and y
    """
    x_dim = unevenise((int(ceil(x_delt) / resolution_m)))
    y_dim = unevenise(int(ceil(y_delt) / resolution_m))

    rast_pts = np.zeros((x_dim, y_dim, 3))

    for x in range(0, x_dim):
        for y in range(0, y_dim):
            x_delta = x - (x_dim-1) / 2
            y_delta = y - (y_dim-1) / 2
            rast_pts[x, y, :] = [x_med + x_delta, y_med + y_delta, 0]
    return rast_pts


def retrieve_heights(raster, cursor):
    """
    Return a numpy array containing the height
    difference between roof and ground for
    every pixel in the input raster
    """
    height_bsql = """
    WITH line AS (
        SELECT ST_GeomFromText('LINESTRING Z(REPL 190, REPL 4640)', 2056) AS geom
    ), int_gnd AS (
        SELECT DISTINCT CASE
                            WHEN ST_AsText(ST_3dIntersection(a.geom, b.geom)) = text('GEOMETRYCOLLECTION EMPTY') THEN ST_3DClosestPoint(a.geom, b.geom)
                            ELSE ST_3dIntersection(a.geom, b.geom)
                        END AS geom
        FROM line a, grounds b
        WHERE ST_3DIntersects(a.geom, b.geom)
    ), int_roof AS (
        SELECT DISTINCT CASE
                            WHEN ST_AsText(ST_3dIntersection(a.geom, b.geom)) = text('GEOMETRYCOLLECTION EMPTY') THEN ST_3DClosestPoint(a.geom, b.geom)
                            ELSE ST_3dIntersection(a.geom, b.geom)
                        END AS geom
        FROM line a, roofs b
        WHERE ST_3DIntersects(a.geom, b.geom)
    ), alt_gnd AS (
        SELECT CASE
                    WHEN tot_int_g > 1 THEN sumZ / tot_int_g
                    ELSE sumZ
               END as alti
        FROM (SELECT DISTINCT COUNT(*) AS tot_int_g, SUM(ST_Z(geom)) AS sumZ FROM int_gnd) tig, int_gnd b
    ), alt_roof AS (
        SELECT CASE
                    WHEN tot_int_r > 1 THEN sumZ / tot_int_r
                    ELSE sumZ
               END as alti
        FROM (SELECT DISTINCT COUNT(*) AS tot_int_r, SUM(ST_Z(geom)) AS sumZ FROM int_roof) tig, int_roof b
    )
    SELECT r.alti - g.alti AS height
    FROM alt_roof r, alt_gnd g;"""
    for x in range(0, raster.shape[0]):
        for y in range(0, raster.shape[1]):
            sql_q = height_bsql
            coords_text = " ".join(str(e) for e in raster[x, y, 0:2])
            sql_q = sql_q.replace("REPL", coords_text)
            cursor.execute(sql_q)
            if cursor.statusmessage == 'SELECT 0':
                raster[x, y, 2] = 0
            else:
                raster[x, y, 2] = round(cursor.fetchone()[0], 3)
    return raster


def init_raster_big(x_dim, y_dim, x_med, y_med, resolution_m):
    """"
    Returns a raster containing (x,y) coordinates separated by
    resolution_m  metres in the z dimension. The raster is padded
    with two extras rows/column in x and y dimension.
    Example given 128, 128 it will return a numpy array of
    shape (130,130,3) with last dimension being an array [x-coord, y-coord, 0]
    """
    x_dim_r = x_dim + 2
    y_dim_r = y_dim + 2
    rast_pts = np.zeros((x_dim_r, y_dim_r, 3))
    for x in range(0, x_dim_r):
        for y in range(0, y_dim_r):
            x_delta = x - (x_dim_r-1) / 2
            y_delta = y - (y_dim_r-1) / 2
            rast_pts[x, y, :] = [
                x_med + (x_delta * resolution_m), y_med + (y_delta * resolution_m), 0]
    return rast_pts


def retrieve_rast_presence(x_dim, y_dim, x_med, y_med, resolution_m, cursor, table_name):
    """
    Return a numpy array containing the built percentage
    for every pixel of a raster based on the input dimensions
    and resolution
    """
    rast_query = """WITH pix AS (
                    SELECT ST_MakeEnvelope(x_min, y_min, x_max, y_max, 2056) AS geom
                ), intersection AS (
                    SELECT (ST_Dump(ST_Union(ST_Intersection(a.geom, b.geom)))).geom
                    FROM pix a, TABLENAME b
                    WHERE ST_Intersects(a.geom, b.geom)
                ), area AS (
                    SELECT ROUND((ST_Area(geom) / SQUAREAREA)::numeric, 4) AS aire
                    FROM intersection
                )
                SELECT * FROM area"""
    rast_query = rast_query.replace("TABLENAME", str(table_name))
    rast_query = rast_query.replace(
        "SQUAREAREA", str(resolution_m*resolution_m))

    # Create a raster of previously defined size and resolution
    raster = init_raster_big(x_dim, y_dim, x_med, y_med, resolution_m)

    for i in range(1, raster.shape[0]-1):
        for j in range(1, raster.shape[0]-1):

            xmin, ymin = raster[i-1, j-1, :2]
            xmax, ymax = raster[i+1, j+1, :2]

            sql_q = rast_query
            sql_q = sql_q.replace("x_min", str(xmin + (resolution_m/2)))
            sql_q = sql_q.replace("y_min", str(ymin + (resolution_m/2)))
            sql_q = sql_q.replace("x_max", str(xmax - (resolution_m/2)))
            sql_q = sql_q.replace("y_max", str(ymax - (resolution_m/2)))

            cursor.execute(sql_q)
            if cursor.statusmessage != 'SELECT 0':
                raster[i, j, 2] = cursor.fetchone()[0]

    raster = raster[1:x_dim+1, 1:y_dim+1, 2]
    return raster


def total_nb_raster(raster_dict):
    """
    Return the total number of rasters
    contained in the dictionnary
    """
    tot_ras = 0
    for i in raster_dict.keys():
        if raster_dict[i] is not None:
            tot_ras += len(raster_dict[i])
    return tot_ras


# Computation part

# Retrieve the json path
json_files_to_convert = []
for fname in os.listdir(args.input_folder_path):
    json_files_to_convert.append(os.path.join(args.input_folder_path, fname))
json_files_to_convert = sorted(json_files_to_convert)

# print(*json_files_to_convert, sep='\n')

# Create two hdf5 files
cat_file = h5py.File(os.path.join(args.output_folder_path,
                                  'categories' + args.filename_suffix + '.hdf5'), 'w')
class_file = h5py.File(os.path.join(
    args.output_folder_path, 'classes' + args.filename_suffix + '.hdf5'), 'w')

cat_file.create_dataset("rasters_cat", shape=(
    1, 128, 128, 3), maxshape=(None, 128, 128, 3), dtype="float")
cat_file.create_dataset("labels_cat", shape=(
    1, ), maxshape=(None, ), dtype="int")

class_file.create_dataset("rasters_class", shape=(
    1, 128, 128, 3), maxshape=(None, 128, 128, 3), dtype="float")
class_file.create_dataset("labels_class", shape=(1, ),
                          maxshape=(None, ), dtype="int")

cat_file.close()
class_file.close()

# Iterate on each json file
for path in json_files_to_convert:
    # Print file name and starting time
    print(f"Starting file {os.path.split(path)[-1]} - {datetime.now()}")

    # Initialise the variables
    results_rast_cat, results_rast_class = {}, {}
    final_result_cat, final_result_class = {}, {}
    possible_cats = ['1020', '1030', '1040', '1060', '1080']
    possible_classes = ['1110', '1121', '1122', '1130', '1211', '1212', '1220',
                        '1230', '1231', '1241', '1242', '1251', '1252', '1261',
                        '1262', '1263', '1264', '1265', '1271', '1272', '1273', '1274']
    for i in possible_cats:
        results_rast_cat[i] = []
        final_result_cat[i] = None

    for i in possible_classes:
        results_rast_class[i] = []
        final_result_class[i] = None

    # Retrieve the citymodel
    cm = cityjson.load(path)
    buildings = cm.get_cityobjects(type=["Building", "BuildingPart"])
    # Keep a set of building installation
    building_installation_list = set(
        cm.get_cityobjects(type=["BuildingInstallation"]).keys())
    # DB connection
    connection_string = "dbname='" + args.db_name + "' user='" + args.db_username + "' host='localhost' password='" + args.db_password + \
        "'" if args.db_password is not None else "dbname='" + args.db_name + \
        "' user='" + args.db_username + "' host='localhost'"
    conn = db.connect(connection_string)
    cur = conn.cursor()

    # Subset geb, bdg and roads
    subset_table_from_cm_bbox(cm, cur, conn, 'geb')
    subset_table_from_cm_bbox(cm, cur, conn, 'bdg')
    subset_table_from_cm_bbox(cm, cur, conn, 'hway_poly')

    # Make sure roof and ground tables are clean
    remove_rows_from_db(cur, conn)

    for i in buildings:
        # Identify and retrieve building
        # print(i)
        b = buildings[i]

        # Caracterise building
        has_child = True if len(b.children) > 0 else False
        has_parent = True if len(b.parents) > 0 else False
        has_geom = True if len(b.geometry) > 0 else False
        valid = True if has_geom or (not has_geom and has_child) else False

        # Make sure previous buildings geometries are removed
        remove_rows_from_db(cur, conn)

        if not valid:
            # Building can't be used
            # stats_non_valid+=1
            # print("Aborting, cause invalid")
            # print("")
            continue
        elif has_parent:
            # Children are analyzed at their parent level
            # stats_children+=1
            # print("Aborting, cause is a child")
            # print("")
            continue
        elif has_child:
            # Children need to be analyzed first
            # stats_parent+=1
            # print("Need further computing")
            # print("")

            # CHILDREN

            #### Children evaluation part ####
            children_cats = []
            children_classes = []

            children = list(b.children)
            # Remove ids of building installation
            for child in list(children):
                if child in building_installation_list:
                    children.remove(child)
                    # print("install removed")

            # Retrieve class and category of the children
            for child in list(children):
                cb = buildings[child]
                # check if child has a geometry
                if len(cb.geometry) == 0:
                    # If not, don't consider this child anymore
                    children.remove(child)
                    continue
                # Retrieve the cat and class
                catNclass = get_cat_and_class_from_building(
                    cb, cur, conn, delete_all_rows=True)

                # If problem, remove child
                if len(catNclass) < 2 and catNclass[0] in ['NoGeom', 'NoRoofOrGround']:
                    children.remove(child)
                    continue
                # elif Nocat assign to cat and class
                elif len(catNclass) < 2:
                    children_cats.append(catNclass[0])
                    children_classes.append(catNclass[0])
                # else assign to their respective list
                else:
                    children_cats.append(catNclass[0])
                    children_classes.append(catNclass[1])

            # Retrieve sets of unique elements
            cats_without_NoCat = list(set(children_cats))
            if 'NoCat' in children_cats:
                cats_without_NoCat.remove('NoCat')
            if 'MultipleCat' in children_cats:
                cats_without_NoCat.remove('MultipleCat')

            class_wo_NoClass = list(set(children_classes))
            if 'NoCat' in children_classes:
                class_wo_NoClass.remove('NoCat')
            if 'NoClass' in children_classes:
                class_wo_NoClass.remove('NoClass')
            if 'MultipleCat' in children_classes:
                class_wo_NoClass.remove('MultipleCat')

            #### Children assignment part ####

            # No more children geometries

            if len(children) == 0 and not has_geom:
                # print("Aborting, cause no child and no geom")
                continue
            # No children and a geom -> simple building
            elif len(children) == 0 and has_geom:
                # simple building
                catNclass = get_cat_and_class_from_building(b, cur, conn)
                if len(catNclass) < 2:
                    # print("Aborting, cause {}".format(catNclass[0]))
                    continue
                else:
                    # Compute the coordinates
                    x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                    # Initialise the building's raster
                    rast_bd = init_rast(1, x_diff, y_diff, x_med, y_med)
                    if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                        # Retrieve heights
                        rast_height = retrieve_heights(rast_bd, cur)
                        if np.any(np.unique(rast_height[:, :, 2])) != 0:
                            # Create placeholder
                            final_rast = np.zeros((128, 128, 3))

                            # Non-empty raster to grow
                            xstart = 64 - (rast_height.shape[0]-1)/2
                            ystart = 64 - (rast_height.shape[1]-1)/2
                            for x in range(rast_height.shape[0]):
                                for y in range(rast_height.shape[1]):
                                    final_rast[int(
                                        xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                            # Retrieve built and road environment
                            final_rast[:, :, 1] = retrieve_rast_presence(
                                128, 128, x_med, y_med, 4, cur, "bdg_zone")
                            final_rast[:, :, 2] = retrieve_rast_presence(
                                128, 128, x_med, y_med, 3, cur, "hway_poly_zone")

                            # Add to results for cat
                            results_rast_cat[str(catNclass[0])].append(
                                final_rast)
                            # Add result for class
                            if catNclass[1] != 'NoClass':
                                results_rast_class[str(catNclass[1])].append(
                                    final_rast)

                    continue

            # Same children's category

            if len(set(children_cats)) == 1 or len(cats_without_NoCat) == 1:

                # Only category is None
                if len(cats_without_NoCat) == 0:
                    if not has_geom:
                        # if children don't have a cat and parent hasn't too
                        # print("Aborting, cause parent and children have NoCat")
                        continue
                    else:
                        # Else use cat of parent for all parts
                        catNclass = get_cat_and_class_from_building(b, cur, conn)
                        if len(catNclass) < 2:
                            # print("Aborting, cause parent has {} and children have NoCat".format(catNclass[0]))
                            continue
                        else:
                            # Add geometries of children to db tables
                            for num in range(len(children)):
                                if children_cats[num] not in ['NoGeom', 'NoRoofOrGround']:
                                    cb = buildings[children[num]]
                                    get_cat_and_class_from_building(
                                        cb, cur, conn, delete_norslt_rows=False)
                            # Compute the coordinates of building made of all parts
                            x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                            # Initialise the building's raster
                            rast_bd = init_rast(
                                1, x_diff, y_diff, x_med, y_med)
                            if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                                # Retrieve heights
                                rast_height = retrieve_heights(rast_bd, cur)
                                if np.any(np.unique(rast_height[:, :, 2])) != 0:
                                    # Create placeholder
                                    final_rast = np.zeros((128, 128, 3))

                                    # Non-empty raster to grow
                                    xstart = 64 - (rast_height.shape[0]-1)/2
                                    ystart = 64 - (rast_height.shape[1]-1)/2
                                    for x in range(rast_height.shape[0]):
                                        for y in range(rast_height.shape[1]):
                                            final_rast[int(
                                                xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                                    # Retrieve built and road environment
                                    final_rast[:, :, 1] = retrieve_rast_presence(
                                        128, 128, x_med, y_med, 4, cur, "bdg_zone")
                                    final_rast[:, :, 2] = retrieve_rast_presence(
                                        128, 128, x_med, y_med, 3, cur, "hway_poly_zone")

                                    # Add to results for cat
                                    results_rast_cat[str(catNclass[0])].append(
                                        final_rast)
                                    # Add result for class
                                    if catNclass[1] != 'NoClass':
                                        results_rast_class[str(catNclass[1])].append(
                                            final_rast)
                            continue

                # Only one numerical category
                else:
                    if not has_geom:
                        # If building hasn't any geom use the children cat
                        cat = cats_without_NoCat[0]
                    else:
                        # Check if children and parent have same cat
                        catNclass = get_cat_and_class_from_building(b, cur, conn)
                        # If parent has no geom continue to children
                        if len(catNclass) < 2:
                            if catNclass[0] in ['NoGeom', 'NoRoofOrGround', 'MultipleCat']:
                                remove_rows_from_db(cur, conn)
                        # If p and c do not share the same cat, add a raster for parent
                        elif catNclass[0] != list(set(children_cats))[0]:
                            # Compute the coordinates of building
                            x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                            # Initialise the building's raster
                            rast_bd = init_rast(
                                1, x_diff, y_diff, x_med, y_med)
                            if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                                # Retrieve heights
                                rast_height = retrieve_heights(rast_bd, cur)
                                if np.any(np.unique(rast_height[:, :, 2])) != 0:
                                    # Create placeholder
                                    final_rast = np.zeros((128, 128, 3))

                                    # Non-empty raster to grow
                                    xstart = 64 - (rast_height.shape[0]-1)/2
                                    ystart = 64 - (rast_height.shape[1]-1)/2
                                    for x in range(rast_height.shape[0]):
                                        for y in range(rast_height.shape[1]):
                                            final_rast[int(
                                                xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                                    # Retrieve built and road environment
                                    final_rast[:, :, 1] = retrieve_rast_presence(
                                        128, 128, x_med, y_med, 4, cur, "bdg_zone")
                                    final_rast[:, :, 2] = retrieve_rast_presence(
                                        128, 128, x_med, y_med, 3, cur, "hway_poly_zone")
                                    # Add to results for cat
                                    results_rast_cat[str(catNclass[0])].append(
                                        final_rast)
                                    # Add result for class
                                    if catNclass[1] != 'NoClass':
                                        results_rast_class[str(catNclass[1])].append(
                                            final_rast)

                            # Remove building geometries to make way for children geoms
                            remove_rows_from_db(cur, conn)

                    # Add geometries of children to db tables
                    for num in range(len(children)):
                        if children_cats[num] not in ['NoGeom', 'NoRoofOrGround']:
                            cb = buildings[children[num]]
                            get_cat_and_class_from_building(
                                cb, cur, conn, delete_norslt_rows=False)

                    # Compute the coordinates of parts in the db
                    x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                    # Initialise the building's raster
                    rast_bd = init_rast(1, x_diff, y_diff, x_med, y_med)
                    if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                        # Retrieve heights
                        rast_height = retrieve_heights(rast_bd, cur)
                        if np.any(np.unique(rast_height[:, :, 2])) != 0:
                            # Create placeholder
                            final_rast = np.zeros((128, 128, 3))

                            # Non-empty raster to grow
                            xstart = 64 - (rast_height.shape[0]-1)/2
                            ystart = 64 - (rast_height.shape[1]-1)/2
                            for x in range(rast_height.shape[0]):
                                for y in range(rast_height.shape[1]):
                                    final_rast[int(
                                        xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                            # Retrieve built and road environment
                            final_rast[:, :, 1] = retrieve_rast_presence(
                                128, 128, x_med, y_med, 4, cur, "bdg_zone")
                            final_rast[:, :, 2] = retrieve_rast_presence(
                                128, 128, x_med, y_med, 3, cur, "hway_poly_zone")
                            # Add to results for cat
                            results_rast_cat[str(cat)].append(final_rast)
                            # Add to results for class
                            if class_wo_NoClass == 1:
                                results_rast_class[str(class_wo_NoClass[0])].append(
                                    final_rast)
                                continue
                    # Remove rows befor continuing
                    remove_rows_from_db(cur, conn)

            # Multiple categories
            else:
                child_diff_cat = {}
                for cat in cats_without_NoCat:
                    idx = [i for i, x in enumerate(children_cats) if x == cat]
                    child_diff_cat[cat] = idx

                # No parent geometry
                if not has_geom:
                    # Add all buildings (exception de NoCat)
                    for cat in cats_without_NoCat:
                        for bat in child_diff_cat[cat]:
                            cb = buildings[children[bat]]
                            get_cat_and_class_from_building(
                                cb, cur, conn, delete_norslt_rows=False)
                        # Compute the coordinates of parts in the db
                        x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                        # Initialise the building's raster
                        rast_bd = init_rast(1, x_diff, y_diff, x_med, y_med)
                        if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                            # Retrieve heights
                            rast_height = retrieve_heights(rast_bd, cur)
                            if np.any(np.unique(rast_height[:, :, 2])) != 0:
                                # Create placeholder
                                final_rast = np.zeros((128, 128, 3))

                                # Non-empty raster to grow
                                xstart = 64 - (rast_height.shape[0]-1)/2
                                ystart = 64 - (rast_height.shape[1]-1)/2
                                for x in range(rast_height.shape[0]):
                                    for y in range(rast_height.shape[1]):
                                        final_rast[int(
                                            xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                                # Retrieve built and road environment
                                final_rast[:, :, 1] = retrieve_rast_presence(
                                    128, 128, x_med, y_med, 4, cur, "bdg_zone")
                                final_rast[:, :, 2] = retrieve_rast_presence(
                                    128, 128, x_med, y_med, 3, cur, "hway_poly_zone")

                                # Add to results for cat
                                results_rast_cat[str(cat)].append(final_rast)

                        # Remove geometries before continuing
                        remove_rows_from_db(cur, conn)

                # Parent has a geometry
                else:
                    catNclass_parent = get_cat_and_class_from_building(
                        b, cur, conn, delete_all_rows=True)
                    # If parent has cat n class
                    if len(catNclass_parent) > 1:
                        # Create a raster for each category
                        for cat in cats_without_NoCat:
                            # If cat of children is the same as parent, add p geom
                            if cat == catNclass_parent[0]:
                                # Add geometries to db table
                                get_cat_and_class_from_building(
                                    b, cur, conn, delete_norslt_rows=False)
                            # Add all children geom corresponding to cat
                            for bat in child_diff_cat[cat]:
                                cb = buildings[children[bat]]
                                get_cat_and_class_from_building(
                                    cb, cur, conn, delete_norslt_rows=False)
                            # Compute the coordinates of parts in the db
                            x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                            # Initialise the building's raster
                            rast_bd = init_rast(
                                1, x_diff, y_diff, x_med, y_med)
                            if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                                # Retrieve heights
                                rast_height = retrieve_heights(rast_bd, cur)
                                if np.any(np.unique(rast_height[:, :, 2])) != 0:
                                    # Create placeholder
                                    final_rast = np.zeros((128, 128, 3))

                                    # Non-empty raster to grow
                                    xstart = 64 - (rast_height.shape[0]-1)/2
                                    ystart = 64 - (rast_height.shape[1]-1)/2
                                    for x in range(rast_height.shape[0]):
                                        for y in range(rast_height.shape[1]):
                                            final_rast[int(
                                                xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                                    # Retrieve built and road environment
                                    final_rast[:, :, 1] = retrieve_rast_presence(
                                        128, 128, x_med, y_med, 4, cur, "bdg_zone")
                                    final_rast[:, :, 2] = retrieve_rast_presence(
                                        128, 128, x_med, y_med, 3, cur, "hway_poly_zone")

                                    # Add to results for cat
                                    results_rast_cat[str(cat)].append(
                                        final_rast)

                            # Remove geometries before continuing
                            remove_rows_from_db(cur, conn)

            # Multiple class for children
            if len(class_wo_NoClass) > 1:

                child_diff_class = {}
                for klass in class_wo_NoClass:
                    idx = [i for i, x in enumerate(
                        children_classes) if x == klass]
                    child_diff_class[klass] = idx

                # No parent geometry
                if not has_geom:
                    # Add buildings for each class (exception de NoClass)
                    for klass in class_wo_NoClass:
                        for bat in child_diff_class[klass]:
                            cb = buildings[children[bat]]
                            get_cat_and_class_from_building(
                                cb, cur, conn, delete_norslt_rows=False)
                        # Compute the coordinates of parts in the db
                        x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                        # Initialise the building's raster
                        rast_bd = init_rast(1, x_diff, y_diff, x_med, y_med)
                        if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                            # Retrieve heights
                            rast_height = retrieve_heights(rast_bd, cur)
                            if np.any(np.unique(rast_height[:, :, 2])) != 0:
                                # Create placeholder
                                final_rast = np.zeros((128, 128, 3))

                                # Non-empty raster to grow
                                xstart = 64 - (rast_height.shape[0]-1)/2
                                ystart = 64 - (rast_height.shape[1]-1)/2
                                for x in range(rast_height.shape[0]):
                                    for y in range(rast_height.shape[1]):
                                        final_rast[int(
                                            xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                                # Retrieve built and road environment
                                final_rast[:, :, 1] = retrieve_rast_presence(
                                    128, 128, x_med, y_med, 4, cur, "bdg_zone")
                                final_rast[:, :, 2] = retrieve_rast_presence(
                                    128, 128, x_med, y_med, 3, cur, "hway_poly_zone")

                                # Add to results for class
                                results_rast_class[str(klass)].append(
                                    final_rast)

                        # Remove geometries before continuing
                        remove_rows_from_db(cur, conn)

                # Parent geometry
                else:
                    catNclass_parent = get_cat_and_class_from_building(
                        b, cur, conn, delete_all_rows=True)
                    # If parent has cat n class
                    if len(catNclass_parent) > 1:
                        # Create a raster for each category
                        for klass in class_wo_NoClass:
                            # If cat of children is the same as parent, add p geom
                            if klass == catNclass_parent[1]:
                                # Add geometries to db table
                                get_cat_and_class_from_building(
                                    b, cur, conn, delete_norslt_rows=False)
                            # Add all children geom corresponding to cat
                            for bat in child_diff_class[klass]:
                                cb = buildings[children[bat]]
                                get_cat_and_class_from_building(
                                    cb, cur, conn, delete_norslt_rows=False)
                            # Compute the coordinates of parts in the db
                            x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                            # Initialise the building's raster
                            rast_bd = init_rast(
                                1, x_diff, y_diff, x_med, y_med)
                            if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                                # Retrieve heights
                                rast_height = retrieve_heights(rast_bd, cur)
                                if np.any(np.unique(rast_height[:, :, 2])) != 0:
                                    # Create placeholder
                                    final_rast = np.zeros((128, 128, 3))

                                    # Non-empty raster to grow
                                    xstart = 64 - (rast_height.shape[0]-1)/2
                                    ystart = 64 - (rast_height.shape[1]-1)/2
                                    for x in range(rast_height.shape[0]):
                                        for y in range(rast_height.shape[1]):
                                            final_rast[int(
                                                xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                                    # Retrieve built and road environment
                                    final_rast[:, :, 1] = retrieve_rast_presence(
                                        128, 128, x_med, y_med, 4, cur, "bdg_zone")
                                    final_rast[:, :, 2] = retrieve_rast_presence(
                                        128, 128, x_med, y_med, 3, cur, "hway_poly_zone")

                                    # Add to results for class
                                    results_rast_class[str(klass)].append(
                                        final_rast)

                            # Remove geometries before continuing
                            remove_rows_from_db(cur, conn)
            # End of children
            continue

        # SIMPLE BUILDING
        else:
            # Simple building (no parent, no child, a geom)
            # stats_simple+=1
            # print("Single building")

            # Retrieve cat and class of building
            catNclass = get_cat_and_class_from_building(b, cur, conn)

            # Check if failure cause or results were returned
            if len(catNclass) < 2:
                # print(f"Aborting, cause {catNclass[0]}")
                # stat_simple_fail+=1
                # failed.append(i)
                # print("")
                # If no cat/class, go to next building
                continue
            # If results were returned
            else:
                # Compute the coordinates
                x_diff, x_med, y_diff, y_med = bbox_helpers_db(cur)
                # Initialise the building's raster
                rast_bd = init_rast(1, x_diff, y_diff, x_med, y_med)

                if rast_bd.shape[0] < 127 and rast_bd.shape[1] < 127:
                    # Retrieve heights
                    rast_height = retrieve_heights(rast_bd, cur)
                    if np.any(np.unique(rast_height[:, :, 2])) != 0:
                        # Create placeholder
                        final_rast = np.zeros((128, 128, 3))

                        # Non-empty raster to grow
                        xstart = 64 - (rast_height.shape[0]-1)/2
                        ystart = 64 - (rast_height.shape[1]-1)/2
                        for x in range(rast_height.shape[0]):
                            for y in range(rast_height.shape[1]):
                                final_rast[int(
                                    xstart + x), int(ystart + y), 0] = rast_height[x, y, 2]

                        # Retrieve built and road environment
                        final_rast[:, :, 1] = retrieve_rast_presence(
                            128, 128, x_med, y_med, 4, cur, "bdg_zone")
                        final_rast[:, :, 2] = retrieve_rast_presence(
                            128, 128, x_med, y_med, 3, cur, "hway_poly_zone")

                        # Add to results for cat
                        results_rast_cat[str(catNclass[0])].append(final_rast)
                        # Add result for class
                        if catNclass[1] != 'NoClass':
                            results_rast_class[str(catNclass[1])].append(
                                final_rast)

                # Erase table for next iter
                remove_rows_from_db(cur, conn)

    cur.close()
    conn.close()
    #
    # Write to hdf5
    #
    # Adjust results files (remove empty cat/class)
    results_rast_cat = {k: np.array(v)
                        for k, v in results_rast_cat.items() if v}
    results_rast_class = {k: np.array(v)
                          for k, v in results_rast_class.items() if v}

    # Get the total number of rasters obtained
    tot_rast_cat = total_nb_raster(results_rast_cat)
    tot_rast_class = total_nb_raster(results_rast_class)

    # Write to file only if a raster was created
    if tot_rast_cat:
        # Open file connections
        cat_file = h5py.File(os.path.join(
            args.output_folder_path, 'categories' + args.filename_suffix + '.hdf5'), 'r+')
        class_file = h5py.File(os.path.join(
            args.output_folder_path, 'classes' + args.filename_suffix + '.hdf5'), 'r+')

        # Retrieve actual shape of hdf5 cat
        prev_cat_shape = cat_file["rasters_cat"].shape[0]

        # Check if it is a new empty hdf5
        if np.all(cat_file["rasters_cat"][0] == 0.):
            # Adjust correction accordingly
            new_hdf5_corr = 1
            prev_cat_shape -= 1
        else:
            new_hdf5_corr = 0

        # Resize the files to host the new rasters
        cat_file["rasters_cat"].resize(
            cat_file["rasters_cat"].shape[0] - new_hdf5_corr + tot_rast_cat, 0)
        cat_file["labels_cat"].resize(
            cat_file["labels_cat"].shape[0] - new_hdf5_corr + tot_rast_cat, 0)

        # Retrieve the previous shape as starting point
        prev_idx = prev_cat_shape
        # Iterate on all cats
        for k in results_rast_cat.keys():
            # If array is sufficently small write it directly
            if len(results_rast_cat[k]) < 10000:
                idx = prev_idx + len(results_rast_cat[k])
                cat_file["rasters_cat"][prev_idx:idx,
                                        :, :, :] = results_rast_cat[k]
                cat_file["labels_cat"][prev_idx:idx] = int(k)
                prev_idx += len(results_rast_cat[k])
            # if not iterate on smaller bits of 10000
            else:
                prev_step = 0
                for step in range(10000, len(results_rast_cat[k]), 10000):
                    idx = prev_idx + 10000
                    cat_file["rasters_cat"][prev_idx:idx, :, :,
                                            :] = results_rast_cat[k][prev_step:step]
                    cat_file["labels_cat"][prev_idx:idx] = int(k)
                    prev_step += step
                    prev_idx = idx
                if len(results_rast_cat[k]) % 10000 != 0:
                    leftover = len(results_rast_cat[k]) % 10000
                    idx += leftover
                    cat_file["rasters_cat"][prev_idx:idx, :, :,
                                            :] = results_rast_cat[k][step:step+leftover]
                    cat_file["labels_cat"][prev_idx:idx] = int(k)
                    prev_idx = idx

        # Retrieve actual shape of hdf5 class
        prev_class_shape = class_file["rasters_class"].shape[0]

        # Check if it is a new empty hdf5
        if np.all(class_file["rasters_class"][0] == 0.):
            new_hdf5_corr = 1
            prev_class_shape -= 1
        else:
            new_hdf5_corr = 0

        # Resize the files to host the new rasters
        class_file["rasters_class"].resize(
            class_file["rasters_class"].shape[0] - new_hdf5_corr + tot_rast_class, 0)
        class_file["labels_class"].resize(
            class_file["labels_class"].shape[0] - new_hdf5_corr + tot_rast_class, 0)
        prev_idx = prev_class_shape

        # Iterate on all cats
        for k in results_rast_class.keys():
            # If array is sufficently small write it directly
            if len(results_rast_class[k]) < 10000:
                idx = prev_idx + len(results_rast_class[k])
                class_file["rasters_class"][prev_idx:idx,
                                            :, :, :] = results_rast_class[k]
                class_file["labels_class"][prev_idx:idx] = int(k)
                prev_idx += len(results_rast_class[k])
            # if not iterate on smaller bits of 10000
            else:
                prev_step = 0
                for step in range(10000, len(results_rast_class[k]), 10000):
                    idx = prev_idx + 10000
                    class_file["rasters_class"][prev_idx:idx, :, :,
                                                :] = results_rast_class[k][prev_step:step]
                    class_file["labels_class"][prev_idx:idx] = int(k)
                    prev_step += step
                    prev_idx = idx
                if len(results_rast_class[k]) % 10000 != 0:
                    leftover = len(results_rast_class[k]) % 10000
                    idx += leftover
                    class_file["rasters_class"][prev_idx:idx, :, :,
                                                :] = results_rast_class[k][step:step+leftover]
                    class_file["labels_class"][prev_idx:idx] = int(k)
                    prev_idx = idx

        cat_file.close()
        class_file.close()

    # Timestamp progress
    print(f"File {os.path.split(path)[-1]} finished - {datetime.now()}")
