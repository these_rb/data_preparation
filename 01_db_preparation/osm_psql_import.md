# Database preparation

Prepare a postgis enabled DB with roads and buildings stored as polygons

#### DB creation and extension loading
```bash
sudo -u postgres psql
CREATE DATABASE osm;
\c osm
CREATE EXTENSION postgis;
```

#### OSM data import with `osm2pgsql`

Different setups can be used according to the `style` file to conserve precise `keys`.

OSM2pgsql (v. >= 1.3) has a new "flex" backend which can be more finely tuned using Lua. It is possible to define exactly what and how you want in which table thus avoiding a lot of post-processing in postgreSQL.

```bash
wget -O osm_switz.osm.pbf https://download.geofabrik.de/europe/switzerland-latest.osm.pbf
sudo -u postgres osm2pgsql osm_switz.osm.pbf -d osm -U postgres -P 5432 -S test.style -C 20000

# sudo -u postgres osm2pgsql grand_test.osm -d osm2 -U postgres -P 5432 -S test.style -C 20000
```

#### Buildings processing (CH1903+)
```sql
SELECT * INTO bdg FROM planet_osm_polygon WHERE building IS NOT NULL;

ALTER TABLE bdg
DROP COLUMN highway,
DROP COLUMN tunnel,
DROP COLUMN lanes,
DROP COLUMN width,
DROP COLUMN tracktype,
DROP COLUMN oneway;
--DROP COLUMN service;

-- Remove duplicates
ALTER TABLE bdg ADD COLUMN id SERIAL;
DELETE
FROM
    bdg a
        USING bdg b
WHERE
    a.id < b.id
    AND a.osm_id = b.osm_id;

-- Create new geom in swiss coordinates and drop previous
SELECT AddGeometryColumn ('public', 'bdg', 'geom', 2056, 'POLYGON', 2);
UPDATE bdg
SET geom = ST_Transform(way, 2056);
ALTER TABLE bdg
DROP COLUMN way,
DROP COLUMN id;

-- Make sure polygons are valid
UPDATE bdg
SET geom = ST_MakeValid(geom) WHERE NOT ST_IsValid(geom);

-- Create index on buildings geometries
CREATE INDEX ON bdg USING gist(geom);
VACUUM ANALYZE bdg;
```

#### Processing roads from lines to polygons (CH1903+)

```sql
-- Retrieve highways
SELECT * INTO hway FROM planet_osm_roads
WHERE highway IS NOT NULL;

-- Retrieve highways
INSERT INTO hway SELECT * FROM planet_osm_line
WHERE highway IS NOT NULL;

-- Drop unwanted column
ALTER TABLE hway
DROP COLUMN building;

-- Remove unwanted features
DELETE FROM hway
WHERE highway NOT IN ('motorway', 'motorway_link',
                      'trunk', 'trunk_link',
                      'primary', 'primary_link',
                      'secondary', 'secondary_link',
                      'tertiary', 'tertiary_link',
                      'residential', 'living_street', 'pedestrian',
                      'service', 'unclassified', 'track');

-- remove undefined tracktypes
DELETE FROM hway
WHERE highway = 'track' AND
(tracktype NOT IN ('grade1', 'grade2', 'grade3') OR tracktype IS NULL);

-- Remove part of way located underground
DELETE FROM hway
WHERE tunnel = 'yes';
ALTER TABLE hway DROP COLUMN tunnel;

-- Remove duplicates
ALTER TABLE hway ADD COLUMN id SERIAL;
DELETE
FROM
    hway a
        USING hway b
WHERE
    a.id < b.id
    AND a.osm_id = b.osm_id;

-- Create new geometry in swiss coordinates and delete previous
SELECT AddGeometryColumn ('public', 'hway', 'geom', 2056, 'LINESTRING', 2);
UPDATE hway
SET geom = ST_Transform(way, 2056);
ALTER TABLE hway
DROP COLUMN way;

-- Add column for buffering size
ALTER TABLE hway ADD COLUMN buffsize REAL;

-- Trim the width from trailing m
UPDATE hway
SET width = RTRIM(width, 'm');

-- Prevent casting errors
CREATE OR REPLACE FUNCTION cast_to_real(text) returns real as $$
BEGIN
    return cast($1 as real);
exception
    WHEN invalid_text_representation THEN
        return NULL;
END;
$$ language plpgsql immutable;



-- If width is given, buffer of half the size
UPDATE hway
SET buffsize = CASE
                WHEN cast_to_real(width) > 4 THEN cast_to_real(width)/2
                ELSE 2
               END
WHERE width IS NOT NULL;

-- If bidirectional and no lines
UPDATE hway
SET buffsize = CASE
                WHEN highway = 'motorway' THEN 12
                WHEN highway = 'motorway_link' THEN 6
                WHEN highway = 'trunk' THEN 10
                WHEN highway = 'trunk_link' THEN 6
                WHEN highway = 'primary' THEN 7
                WHEN highway = 'primary_link' THEN 3.5
                WHEN highway = 'secondary' THEN 5
                WHEN highway = 'secondary_link' THEN 2.5
                WHEN highway = 'tertiary' THEN 3.5
                WHEN highway = 'tertiary_link' THEN 2
                WHEN highway IN ('unclassified', 'living_street', 'residential') THEN 2.25
                WHEN highway IN ('service', 'track', 'pedestrian') THEN 2
               END
WHERE width IS NULL;

-- Add two metres per additionnal lanes over 2
UPDATE hway
SET buffsize = CASE
                WHEN CAST(lanes AS REAL) > 2 THEN buffsize + (2 * (CAST(lanes AS REAL)-2))
                ELSE buffsize
               END
WHERE width IS NULL AND lanes IS NOT NULL;

-- Cut buffsize in half if one way only
UPDATE hway
SET buffsize = buffsize/2
WHERE buffsize IS NOT NULL
AND oneway = 'yes'
AND highway NOT IN ('unclassified', 'living_street', 'residential', 'service', 'track');

CREATE INDEX ON hway USING gist(geom);
VACUUM ANALYZE hway;

SELECT ST_Buffer(geom, buffsize, 'endcap=square') AS geom, osm_id INTO hway_poly FROM hway;
ALTER TABLE hway_poly
  ALTER COLUMN geom TYPE geometry(POLYGON, 2056)
    USING ST_SetSRID(geom,2056);

-- Make sure geometries are valid
UPDATE hway_poly
SET geom = ST_MakeValid(geom) WHERE NOT ST_IsValid(geom);
-- Add an index
CREATE INDEX ON hway_poly USING gist(geom);
VACUUM ANALYZE hway_poly;
-- Remove original table fomr osm2pgsql import
DROP TABLE hway, planet_osm_line, planet_osm_polygon, planet_osm_point, planet_osm_roads;
```
