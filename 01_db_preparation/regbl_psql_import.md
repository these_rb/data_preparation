## RegBl data import and preparation

We take for granted that the user has set up a DB named `regbl`. This database contains (at least) the `geb` table with all points attributes imported from the RegBl:

```sql
CREATE DATABASE regbl;
\c regbl
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_sfcgal;

-- Create Table for csv import
DROP TABLE IF EXISTS geb;
CREATE TABLE geb(
  egid INT NOT NULL,
  gkode REAL,
  gkodn REAL,
  gkat SMALLINT,
  gklas SMALLINT,
  gstat SMALLINT
);-- Create Table for csv import
DROP TABLE IF EXISTS geb;
CREATE TABLE geb(
  egid INT NOT NULL,
  gkode REAL,
  gkodn REAL,
  gkat SMALLINT,
  gklas SMALLINT,
  gstat SMALLINT
);
```

Prepare csv with R

```R
# With an old .txt format
# geb <- read.delim("Table_GEB-01.txt", header = F, quote = "", fileEncoding = "ISO-8859-1")
# geb_names <- c("EGID", "GEBNR", "GBEZ", "GDEKT", "GDENR", "GDENAME", "GKODE", "GKODN", "GKSCE", "GKAT", "GKLAS", "GSTAT", "GBAUJ", "GBAUM", "GBAUP", "GABBJ", "GAREA", "GVOL", "GVOLNORM", "GVOLSCE", "GASTW", "GANZWHG", "GAZZI", "UPDATE_DATE", "EXPORT_DATE")
# names(geb) <- geb_names
# geb_to_csv <- geb[, c("EGID", "GKODE", "GKODN", "GKAT", "GKLAS", "GSTAT")]
# write.csv(geb_to_csv, "geb_csv.csv", fileEncoding = "UTF-8", row.names = FALSE, na = "NULL")

# With the new dsv format
#options(scipen = 999)
#library(readr)
#gebs <- read_delim("/media/rbubloz/Windows/Users/rbubloz/Documents/Uni/Recherche/Art_1/data/original/GWR_MADD_Export_MADD-20210625-A1_20210630/GWR_MADD_GEB-03_Data_MADD-20210625-A1_20210630.dsv", delim = "\t", guess_max = 1000000L, escape_double = FALSE, trim_ws = TRUE)
#geb_to_csv <- gebs[, c("EGID", "GKODE", "GKODN", "GKAT", "GKLAS", "GSTAT")]
#write.csv(geb_to_csv, "geb_csv.csv", fileEncoding = "UTF-8", row.names = FALSE, na = "NULL")

# With the new .csv that is a .tsv
library(readr)
gebs <- read_tsv("gebaeude_batiment_edificio.csv")
geb_to_csv <- gebs[, c("EGID", "GKODE", "GKODN", "GKAT", "GKLAS", "GSTAT")]
write_csv(geb_to_csv, "geb_csv.csv", na = "NULL")
```

Copy csv in the table `\copy geb FROM 'path/to/geb_csv.csv' DELIMITER ',' HEADER NULL AS 'NULL' CSV`

Add tables and geometry columns in Postgres

```sql
-- Create a geometry column
SELECT AddGeometryColumn('public', 'geb', 'geom', 2056, 'POINT', 2);
UPDATE geb SET geom = ST_SetSRID(ST_MakePoint(gkode, gkodn), 2056);
-- Remove useless data
DELETE FROM geb WHERE geom IS NULL;
DELETE FROM geb WHERE gstat IS NULL;
DELETE FROM geb WHERE gkat IS NULL;
-- Index geometries
CREATE INDEX geb_gix ON geb USING GIST (geom);
VACUUM ANALYZE geb;

-- create tables
CREATE TABLE roofs(id SERIAL);
CREATE TABLE grounds(id SERIAL);
-- add a geometry column
SELECT AddGeometryColumn('public', 'roofs', 'geom', 2056, 'POLYGON', 3);
SELECT AddGeometryColumn('public', 'grounds', 'geom', 2056, 'POLYGON', 3);
```

TODO CHECK WHY 21781 WHY 21781 ???


### Create tables for OSM subsetting

```sql
-- regbl_zone
CREATE TABLE regbl_zone (egid int,
                         gkat smallint,
                         gklas smallint);
SELECT AddGeometryColumn ('public', 'regbl_zone', 'geom', 2056, 'POINT', 2);
CREATE INDEX regbl_geom_index ON regbl_zone USING GIST(geom);

-- bdg_zone
CREATE TABLE bdg_zone (id serial);
SELECT AddGeometryColumn ('public', 'bdg_zone', 'geom', 2056, 'POLYGON', 2);
CREATE INDEX bdg_geom_index ON bdg_zone USING GIST(geom);

-- hway_poly
CREATE TABLE hway_poly_zone (id serial);
SELECT AddGeometryColumn ('public', 'hway_poly_zone', 'geom', 2056, 'POLYGON', 2);
CREATE INDEX hway_geom_index ON hway_poly_zone USING GIST(geom);
```


##### Previous imports - For information only

```sql
-- Create Table for csv import
CREATE TABLE geb(
  egid INT NOT NULL,
  gebnr TEXT,
  gbez TEXT,
  gdekt CHAR(2),
  gdenr SMALLINT,
  gdename TEXT,
  gkode REAL,
  gkodn REAL,
  gksce SMALLINT,
  gkat SMALLINT,
  gklas SMALLINT,
  gstat SMALLINT,
  gbauj SMALLINT,
  gbaum SMALLINT,
  gbaup SMALLINT,
  gabbj SMALLINT,
  garea REAL,
  gvol REAL,
  gvolnorm REAL,
  gvolsce REAL,
  gastw SMALLINT,
  ganzwhg SMALLINT,
  gazzi SMALLINT,
  update_date DATE,
  export_date DATE
);
```


```R
a <- read.delim("Table_GEB-01.txt", header = F, quote = "", fileEncoding = "ISO-8859-1")
a$V24 <- gsub("\\.", "/", a$V24)
a$V25 <- gsub("\\.", "/", a$V25)
write.csv(a, "geb_csv.csv", fileEncoding = "UTF-8", row.names = FALSE, na = "NULL")
```

Copy csv in the table `\copy geb FROM 'path/to/geb_csv.csv' DELIMITER ',' HEADER NULL AS 'NULL' CSV`
