<?xml version="1.0" encoding="UTF-8"?><!--CityGML-Datei erzeugt mit FME Desktop FME(R) 2016.1.3.0 , 29.06.2017 --><core:CityModel xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:bridge="http://www.opengis.net/citygml/bridge/2.0" xmlns:pbase="http://www.opengis.net/citygml/profiles/base/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0">
	<gml:name>swissBUILDINGS3D 2.0</gml:name>
	<gml:boundedBy>
		<gml:Envelope srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
			<gml:lowerCorner>2831255.96 1200678.258 986.219943249436</gml:lowerCorner>
			<gml:upperCorner>2832145.55 1202800.582 1743.73110851454</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<core:cityObjectMember>
		<bldg:Building gml:id="_AC164C12-7BCD-4A55-802E-D133F8527855">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>AC164C12-7BCD-4A55-802E-D133F8527855</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831956.4 1201657.615 1087.10235813453 2831960.404 1201653.106 1087.10238606318 2831959.773 1201658.047 1087.10238275575 2831956.4 1201657.615 1087.10235813453</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831957.087 1201652.24 1087.10236170447 2831960.404 1201653.106 1087.10238606318 2831956.4 1201657.615 1087.10235813453 2831957.087 1201652.24 1087.10236170447</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831957.68 1201652.316 1087.10236604031 2831960.404 1201653.106 1087.10238606318 2831957.087 1201652.24 1087.10236170447 2831957.68 1201652.316 1087.10236604031</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831960.46 1201652.671 1087.10238636192 2831960.404 1201653.106 1087.10238606318 2831957.68 1201652.316 1087.10236604031 2831960.46 1201652.671 1087.10238636192</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831959.773 1201658.047 1087.10238275575 2831956.4 1201657.615 1081.25237725336 2831956.4 1201657.615 1087.10235813453 2831959.773 1201658.047 1087.10238275575</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831959.773 1201658.047 1081.25240172119 2831956.4 1201657.615 1081.25237725336 2831959.773 1201658.047 1087.10238275575 2831959.773 1201658.047 1081.25240172119</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831956.4 1201657.615 1087.10235813453 2831957.087 1201652.24 1081.25238072927 2831957.087 1201652.24 1087.10236170447 2831956.4 1201657.615 1087.10235813453</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831956.4 1201657.615 1081.25237725336 2831957.087 1201652.24 1081.25238072927 2831956.4 1201657.615 1087.10235813453 2831956.4 1201657.615 1081.25237725336</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831957.087 1201652.24 1087.10236170447 2831957.68 1201652.316 1081.2523850381 2831957.68 1201652.316 1087.10236604031 2831957.087 1201652.24 1087.10236170447</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831957.087 1201652.24 1081.25238072927 2831957.68 1201652.316 1081.2523850381 2831957.087 1201652.24 1087.10236170447 2831957.087 1201652.24 1081.25238072927</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831957.68 1201652.316 1087.10236604031 2831960.46 1201652.671 1081.25240523312 2831960.46 1201652.671 1087.10238636192 2831957.68 1201652.316 1087.10236604031</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831957.68 1201652.316 1081.2523850381 2831960.46 1201652.671 1081.25240523312 2831957.68 1201652.316 1087.10236604031 2831957.68 1201652.316 1081.2523850381</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831960.404 1201653.106 1087.10238606318 2831960.404 1201653.106 1081.25240494205 2831959.773 1201658.047 1087.10238275575 2831960.404 1201653.106 1087.10238606318</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831959.773 1201658.047 1087.10238275575 2831960.404 1201653.106 1081.25240494205 2831959.773 1201658.047 1081.25240172119 2831959.773 1201658.047 1087.10238275575</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831960.46 1201652.671 1087.10238636192 2831960.404 1201653.106 1081.25240494205 2831960.404 1201653.106 1087.10238606318 2831960.46 1201652.671 1087.10238636192</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831960.46 1201652.671 1081.25240523312 2831960.404 1201653.106 1081.25240494205 2831960.46 1201652.671 1087.10238636192 2831960.46 1201652.671 1081.25240523312</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831959.773 1201658.047 1081.25240172119 2831960.404 1201653.106 1081.25240494205 2831956.4 1201657.615 1081.25237725336 2831959.773 1201658.047 1081.25240172119</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831956.4 1201657.615 1081.25237725336 2831960.404 1201653.106 1081.25240494205 2831957.087 1201652.24 1081.25238072927 2831956.4 1201657.615 1081.25237725336</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831957.087 1201652.24 1081.25238072927 2831960.404 1201653.106 1081.25240494205 2831957.68 1201652.316 1081.2523850381 2831957.087 1201652.24 1081.25238072927</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831957.68 1201652.316 1081.2523850381 2831960.404 1201653.106 1081.25240494205 2831960.46 1201652.671 1081.25240523312 2831957.68 1201652.316 1081.2523850381</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_37648BFC-5751-401C-8C57-BA9C085A65E6">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Offenes Gebaeude</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>37648BFC-5751-401C-8C57-BA9C085A65E6</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831287.524 1202797.905 1639.6662054135 2831284.59 1202800.582 1639.66622218485 2831286.365 1202796.635 1640.81098560606 2831287.524 1202797.905 1639.6662054135</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831284.59 1202800.582 1639.66622218485 2831283.431 1202799.313 1640.81100240656 2831286.365 1202796.635 1640.81098560606 2831284.59 1202800.582 1639.66622218485</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831283.431 1202799.313 1640.81100240656 2831285.04 1202795.183 1639.66616661503 2831286.365 1202796.635 1640.81098560606 2831283.431 1202799.313 1640.81100240656</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831282.105 1202797.86 1639.66618341103 2831285.04 1202795.183 1639.66616661503 2831283.431 1202799.313 1640.81100240656 2831282.105 1202797.86 1639.66618341103</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831286.365 1202796.635 1640.81098560606 2831286.365 1202796.635 1640.71100322344 2831287.524 1202797.905 1639.6662054135 2831286.365 1202796.635 1640.81098560606</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831286.365 1202796.635 1640.71100322344 2831287.524 1202797.905 1639.56622302983 2831287.524 1202797.905 1639.6662054135 2831286.365 1202796.635 1640.71100322344</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831287.524 1202797.905 1639.6662054135 2831284.59 1202800.582 1639.56623980072 2831284.59 1202800.582 1639.66622218485 2831287.524 1202797.905 1639.6662054135</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831287.524 1202797.905 1639.56622302983 2831284.59 1202800.582 1639.56623980072 2831287.524 1202797.905 1639.6662054135 2831287.524 1202797.905 1639.56622302983</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831284.59 1202800.582 1639.66622218485 2831283.431 1202799.313 1640.71102002348 2831283.431 1202799.313 1640.81100240656 2831284.59 1202800.582 1639.66622218485</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831284.59 1202800.582 1639.56623980072 2831283.431 1202799.313 1640.71102002348 2831284.59 1202800.582 1639.66622218485 2831284.59 1202800.582 1639.56623980072</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831283.431 1202799.313 1640.81100240656 2831283.431 1202799.313 1640.71102002348 2831282.105 1202797.86 1639.66618341103 2831283.431 1202799.313 1640.81100240656</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831283.431 1202799.313 1640.71102002348 2831282.105 1202797.86 1639.56620102915 2831282.105 1202797.86 1639.66618341103 2831283.431 1202799.313 1640.71102002348</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831282.105 1202797.86 1639.66618341103 2831285.04 1202795.183 1639.56618423361 2831285.04 1202795.183 1639.66616661503 2831282.105 1202797.86 1639.66618341103</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831282.105 1202797.86 1639.56620102915 2831285.04 1202795.183 1639.56618423361 2831282.105 1202797.86 1639.66618341103 2831282.105 1202797.86 1639.56620102915</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831285.04 1202795.183 1639.66616661503 2831286.365 1202796.635 1640.71100322344 2831286.365 1202796.635 1640.81098560606 2831285.04 1202795.183 1639.66616661503</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831285.04 1202795.183 1639.66616661503 2831285.04 1202795.183 1639.56618423361 2831286.365 1202796.635 1640.71100322344 2831285.04 1202795.183 1639.66616661503</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831286.365 1202796.635 1640.71100322344 2831284.59 1202800.582 1639.56623980072 2831287.524 1202797.905 1639.56622302983 2831286.365 1202796.635 1640.71100322344</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831286.365 1202796.635 1640.71100322344 2831283.431 1202799.313 1640.71102002348 2831284.59 1202800.582 1639.56623980072 2831286.365 1202796.635 1640.71100322344</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831286.365 1202796.635 1640.71100322344 2831285.04 1202795.183 1639.56618423361 2831283.431 1202799.313 1640.71102002348 2831286.365 1202796.635 1640.71100322344</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831283.431 1202799.313 1640.71102002348 2831285.04 1202795.183 1639.56618423361 2831282.105 1202797.86 1639.56620102915 2831283.431 1202799.313 1640.71102002348</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_3DB71A58-14A3-4695-B22E-FCB67ECC3DAC">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>3DB71A58-14A3-4695-B22E-FCB67ECC3DAC</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831822.956 1200680.729 1087.60191655249 2831829.854 1200682.95 1088.00189232489 2831826.956 1200685.421 1087.60194076484 2831822.956 1200680.729 1087.60191655249</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831825.854 1200678.258 1088.00186798858 2831829.854 1200682.95 1088.00189232489 2831822.956 1200680.729 1087.60191655249 2831825.854 1200678.258 1088.00186798858</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831826.956 1200685.421 1087.60194076484 2831827.012 1200684.716 1087.65493234311 2831822.956 1200680.729 1087.60191655249 2831826.956 1200685.421 1087.60194076484</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831829.854 1200682.95 1088.00189232489 2831827.012 1200684.716 1087.65493234311 2831826.956 1200685.421 1087.60194076484 2831829.854 1200682.95 1088.00189232489</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831822.956 1200680.729 1087.60191655249 2831823.661 1200680.785 1087.65491204572 2831825.854 1200678.258 1088.00186798858 2831822.956 1200680.729 1087.60191655249</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831827.012 1200684.716 1087.65493234311 2831823.661 1200680.785 1087.65491204572 2831822.956 1200680.729 1087.60191655249 2831827.012 1200684.716 1087.65493234311</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831825.854 1200678.258 1088.00186798858 2831825.799 1200678.962 1087.94987624237 2831829.854 1200682.95 1088.00189232489 2831825.854 1200678.258 1088.00186798858</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831823.661 1200680.785 1087.65491204572 2831825.799 1200678.962 1087.94987624237 2831825.854 1200678.258 1088.00186798858 2831823.661 1200680.785 1087.65491204572</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831829.15 1200682.894 1087.94989661589 2831827.012 1200684.716 1087.65493234311 2831829.854 1200682.95 1088.00189232489 2831829.15 1200682.894 1087.94989661589</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831829.854 1200682.95 1088.00189232489 2831825.799 1200678.962 1087.94987624237 2831829.15 1200682.894 1087.94989661589 2831829.854 1200682.95 1088.00189232489</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831829.15 1200682.894 1087.94989661589 2831829.15 1200682.894 1083.06073932649 2831827.012 1200684.716 1087.65493234311 2831829.15 1200682.894 1087.94989661589</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831829.15 1200682.894 1083.06073932649 2831827.012 1200684.716 1083.06072431267 2831827.012 1200684.716 1087.65493234311 2831829.15 1200682.894 1083.06073932649</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831827.012 1200684.716 1087.65493234311 2831823.661 1200680.785 1083.06070407672 2831823.661 1200680.785 1087.65491204572 2831827.012 1200684.716 1087.65493234311</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831827.012 1200684.716 1083.06072431267 2831823.661 1200680.785 1083.06070407672 2831827.012 1200684.716 1087.65493234311 2831827.012 1200684.716 1083.06072431267</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831823.661 1200680.785 1087.65491204572 2831825.799 1200678.962 1083.06071901865 2831825.799 1200678.962 1087.94987624237 2831823.661 1200680.785 1087.65491204572</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831823.661 1200680.785 1083.06070407672 2831825.799 1200678.962 1083.06071901865 2831823.661 1200680.785 1087.65491204572 2831823.661 1200680.785 1083.06070407672</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831825.799 1200678.962 1087.94987624237 2831825.799 1200678.962 1083.06071901865 2831829.15 1200682.894 1087.94989661589 2831825.799 1200678.962 1087.94987624237</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831829.15 1200682.894 1087.94989661589 2831825.799 1200678.962 1083.06071901865 2831829.15 1200682.894 1083.06073932649 2831829.15 1200682.894 1087.94989661589</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831829.15 1200682.894 1083.06073932649 2831823.661 1200680.785 1083.06070407672 2831827.012 1200684.716 1083.06072431267 2831829.15 1200682.894 1083.06073932649</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831829.15 1200682.894 1083.06073932649 2831825.799 1200678.962 1083.06071901865 2831823.661 1200680.785 1083.06070407672 2831829.15 1200682.894 1083.06073932649</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_B9F43AD9-7E83-4E78-A42B-EEE65D7D95DC">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>B9F43AD9-7E83-4E78-A42B-EEE65D7D95DC</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.918 1200690.374 1073.90687852797 2831897.578 1200691.707 1071.07527378302 2831906.758 1200685.614 1071.07534365915 2831909.918 1200690.374 1073.90687852797</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831913.078 1200695.134 1071.07538615111 2831903.896 1200701.227 1071.07531567088 2831909.918 1200690.374 1073.90687852797 2831913.078 1200695.134 1071.07538615111</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831903.896 1200701.227 1071.07531567088 2831900.736 1200696.468 1073.90680807985 2831909.918 1200690.374 1073.90687852797 2831903.896 1200701.227 1071.07531567088</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831900.736 1200696.468 1073.90680807985 2831897.578 1200691.707 1071.07527378302 2831909.918 1200690.374 1073.90687852797 2831900.736 1200696.468 1073.90680807985</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831906.758 1200685.614 1071.07534365915 2831906.617 1200686.306 1071.32229989103 2831909.918 1200690.374 1073.90687852797 2831906.758 1200685.614 1071.07534365915</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831897.578 1200691.707 1071.07527378302 2831906.617 1200686.306 1071.32229989103 2831906.758 1200685.614 1071.07534365915 2831897.578 1200691.707 1071.07527378302</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.918 1200690.374 1073.90687852797 2831912.385 1200694.994 1071.32333848117 2831913.078 1200695.134 1071.07538615111 2831909.918 1200690.374 1073.90687852797</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.445 1200690.563 1073.85488386325 2831909.503 1200690.65 1073.90687532392 2831909.918 1200690.374 1073.90687852797 2831909.445 1200690.563 1073.85488386325</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.918 1200690.374 1073.90687852797 2831906.617 1200686.306 1071.32229989103 2831909.445 1200690.563 1073.85488386325 2831909.918 1200690.374 1073.90687852797</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.503 1200690.65 1073.90687532392 2831912.385 1200694.994 1071.32333848117 2831909.918 1200690.374 1073.90687852797 2831909.503 1200690.65 1073.90687532392</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831913.078 1200695.134 1071.07538615111 2831912.385 1200694.994 1071.32333848117 2831903.896 1200701.227 1071.07531567088 2831913.078 1200695.134 1071.07538615111</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831903.896 1200701.227 1071.07531567088 2831904.036 1200700.535 1071.32227456893 2831900.736 1200696.468 1073.90680807985 2831903.896 1200701.227 1071.07531567088</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831912.385 1200694.994 1071.32333848117 2831904.036 1200700.535 1071.32227456893 2831903.896 1200701.227 1071.07531567088 2831912.385 1200694.994 1071.32333848117</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831900.736 1200696.468 1073.90680807985 2831898.27 1200691.848 1071.32323613873 2831897.578 1200691.707 1071.07527378302 2831900.736 1200696.468 1073.90680807985</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831904.036 1200700.535 1071.32227456893 2831901.153 1200696.191 1073.90681125948 2831900.736 1200696.468 1073.90680807985 2831904.036 1200700.535 1071.32227456893</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831901.153 1200696.191 1073.90681125948 2831898.27 1200691.848 1071.32323613873 2831900.736 1200696.468 1073.90680807985 2831901.153 1200696.191 1073.90681125948</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831898.27 1200691.848 1071.32323613873 2831906.617 1200686.306 1071.32229989103 2831897.578 1200691.707 1071.07527378302 2831898.27 1200691.848 1071.32323613873</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.445 1200690.563 1073.85488386325 2831909.445 1200690.563 1065.63629546234 2831909.503 1200690.65 1073.90687532392 2831909.445 1200690.563 1073.85488386325</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.503 1200690.65 1073.90687532392 2831909.445 1200690.563 1065.63629546234 2831912.385 1200694.994 1071.32333848117 2831909.503 1200690.65 1073.90687532392</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.445 1200690.563 1065.63629546234 2831912.385 1200694.994 1065.63631520208 2831912.385 1200694.994 1071.32333848117 2831909.445 1200690.563 1065.63629546234</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831912.385 1200694.994 1071.32333848117 2831912.385 1200694.994 1065.63631520208 2831904.036 1200700.535 1071.32227456893 2831912.385 1200694.994 1071.32333848117</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831912.385 1200694.994 1065.63631520208 2831904.036 1200700.535 1065.63625159894 2831904.036 1200700.535 1071.32227456893 2831912.385 1200694.994 1065.63631520208</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831904.036 1200700.535 1071.32227456893 2831904.036 1200700.535 1065.63625159894 2831901.153 1200696.191 1073.90681125948 2831904.036 1200700.535 1071.32227456893</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831901.153 1200696.191 1073.90681125948 2831898.27 1200691.848 1065.63621346394 2831898.27 1200691.848 1071.32323613873 2831901.153 1200696.191 1073.90681125948</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831904.036 1200700.535 1065.63625159894 2831898.27 1200691.848 1065.63621346394 2831901.153 1200696.191 1073.90681125948 2831904.036 1200700.535 1065.63625159894</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831898.27 1200691.848 1071.32323613873 2831898.27 1200691.848 1065.63621346394 2831906.617 1200686.306 1071.32229989103 2831898.27 1200691.848 1071.32323613873</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831898.27 1200691.848 1065.63621346394 2831906.617 1200686.306 1065.63627656618 2831906.617 1200686.306 1071.32229989103 2831898.27 1200691.848 1065.63621346394</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831906.617 1200686.306 1071.32229989103 2831909.445 1200690.563 1065.63629546234 2831909.445 1200690.563 1073.85488386325 2831906.617 1200686.306 1071.32229989103</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831906.617 1200686.306 1071.32229989103 2831906.617 1200686.306 1065.63627656618 2831909.445 1200690.563 1065.63629546234 2831906.617 1200686.306 1071.32229989103</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.445 1200690.563 1065.63629546234 2831904.036 1200700.535 1065.63625159894 2831912.385 1200694.994 1065.63631520208 2831909.445 1200690.563 1065.63629546234</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.445 1200690.563 1065.63629546234 2831898.27 1200691.848 1065.63621346394 2831904.036 1200700.535 1065.63625159894 2831909.445 1200690.563 1065.63629546234</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831909.445 1200690.563 1065.63629546234 2831906.617 1200686.306 1065.63627656618 2831898.27 1200691.848 1065.63621346394 2831909.445 1200690.563 1065.63629546234</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_1ED33037-18B3-4733-8AAA-1FFAC5977B22">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>1ED33037-18B3-4733-8AAA-1FFAC5977B22</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831259.965 1202658.554 1743.73110851454 2831258.363 1202656.767 1743.73107873943 2831262.944 1202655.884 1743.73109012682 2831259.965 1202658.554 1743.73110851454</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.363 1202656.767 1743.73107873943 2831258.939 1202651.416 1743.73101551793 2831262.944 1202655.884 1743.73109012682 2831258.363 1202656.767 1743.73107873943</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831255.96 1202654.086 1743.73103398661 2831258.939 1202651.416 1743.73101551793 2831258.363 1202656.767 1743.73107873943 2831255.96 1202654.086 1743.73103398661</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.363 1202656.767 1743.73107873943 2831255.96 1202654.086 1733.09391792313 2831255.96 1202654.086 1743.73103398661 2831258.363 1202656.767 1743.73107873943</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.363 1202656.767 1733.09396241496 2831255.96 1202654.086 1733.09391792313 2831258.363 1202656.767 1743.73107873943 2831258.363 1202656.767 1733.09396241496</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831255.96 1202654.086 1743.73103398661 2831258.939 1202651.416 1733.09389950939 2831258.939 1202651.416 1743.73101551793 2831255.96 1202654.086 1743.73103398661</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831255.96 1202654.086 1733.09391792313 2831258.939 1202651.416 1733.09389950939 2831255.96 1202654.086 1743.73103398661 2831255.96 1202654.086 1733.09391792313</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.939 1202651.416 1743.73101551793 2831262.944 1202655.884 1733.09397368318 2831262.944 1202655.884 1743.73109012682 2831258.939 1202651.416 1743.73101551793</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.939 1202651.416 1733.09389950939 2831262.944 1202655.884 1733.09397368318 2831258.939 1202651.416 1743.73101551793 2831258.939 1202651.416 1733.09389950939</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831262.944 1202655.884 1743.73109012682 2831259.965 1202658.554 1733.09399201643 2831259.965 1202658.554 1743.73110851454 2831262.944 1202655.884 1743.73109012682</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831262.944 1202655.884 1733.09397368318 2831259.965 1202658.554 1733.09399201643 2831262.944 1202655.884 1743.73109012682 2831262.944 1202655.884 1733.09397368318</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831259.965 1202658.554 1743.73110851454 2831259.965 1202658.554 1733.09399201643 2831258.363 1202656.767 1743.73107873943 2831259.965 1202658.554 1743.73110851454</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.363 1202656.767 1743.73107873943 2831259.965 1202658.554 1733.09399201643 2831258.363 1202656.767 1733.09396241496 2831258.363 1202656.767 1743.73107873943</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.363 1202656.767 1733.09396241496 2831258.939 1202651.416 1733.09389950939 2831255.96 1202654.086 1733.09391792313 2831258.363 1202656.767 1733.09396241496</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.363 1202656.767 1733.09396241496 2831262.944 1202655.884 1733.09397368318 2831258.939 1202651.416 1733.09389950939 2831258.363 1202656.767 1733.09396241496</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831258.363 1202656.767 1733.09396241496 2831259.965 1202658.554 1733.09399201643 2831262.944 1202655.884 1733.09397368318 2831258.363 1202656.767 1733.09396241496</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_78889308-BC42-4802-BC04-17903B3D015D">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151016</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151016</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>78889308-BC42-4802-BC04-17903B3D015D</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832088.931 1202091.205 1014.64309588738 2832093.714 1202093.274 1017.91455204656 2832092.938 1202100.766 1014.6431251046 2832088.931 1202091.205 1014.64309588738</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832093.714 1202093.274 1017.91455204656 2832096.742 1202098.692 1017.91457208166 2832092.938 1202100.766 1014.6431251046 2832093.714 1202093.274 1017.91455204656</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.017 1202088.69 1014.64312233339 2832093.714 1202093.274 1017.91455204656 2832088.931 1202091.205 1014.64309588738 2832095.017 1202088.69 1014.64312233339</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832100.341 1202097.179 1014.64315627141 2832093.714 1202093.274 1017.91455204656 2832095.017 1202088.69 1014.64312233339 2832100.341 1202097.179 1014.64315627141</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832100.341 1202097.179 1014.64315627141 2832096.742 1202098.692 1017.91457208166 2832093.714 1202093.274 1017.91455204656 2832100.341 1202097.179 1014.64315627141</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832092.938 1202100.766 1014.6431251046 2832096.742 1202098.692 1017.91457208166 2832100.341 1202097.179 1014.64315627141 2832092.938 1202100.766 1014.6431251046</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832092.938 1202100.766 1014.6431251046 2832093.099 1202099.21 1015.32300598653 2832088.931 1202091.205 1014.64309588738 2832092.938 1202100.766 1014.6431251046</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832100.341 1202097.179 1014.64315627141 2832096.477 1202098.218 1017.91457033327 2832092.938 1202100.766 1014.6431251046 2832100.341 1202097.179 1014.64315627141</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.477 1202098.218 1017.91457033327 2832093.324 1202099.746 1015.27301631263 2832092.938 1202100.766 1014.6431251046 2832096.477 1202098.218 1017.91457033327</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832093.324 1202099.746 1015.27301631263 2832093.099 1202099.21 1015.32300598653 2832092.938 1202100.766 1014.6431251046 2832093.324 1202099.746 1015.27301631263</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832088.931 1202091.205 1014.64309588738 2832089.914 1202091.611 1015.29998678478 2832095.017 1202088.69 1014.64312233339 2832088.931 1202091.205 1014.64309588738</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832093.099 1202099.21 1015.32300598653 2832089.924 1202091.634 1015.3219830318 2832088.931 1202091.205 1014.64309588738 2832093.099 1202099.21 1015.32300598653</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832089.924 1202091.634 1015.3219830318 2832089.914 1202091.611 1015.29998678478 2832088.931 1202091.205 1014.64309588738 2832089.924 1202091.634 1015.3219830318</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.017 1202088.69 1014.64312233339 2832098.936 1202096.351 1015.33602834224 2832100.341 1202097.179 1014.64315627141 2832095.017 1202088.69 1014.64312233339</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832089.914 1202091.611 1015.29998678478 2832094.719 1202089.625 1015.30000766326 2832095.017 1202088.69 1014.64312233339 2832089.914 1202091.611 1015.29998678478</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.719 1202089.625 1015.30000766326 2832094.741 1202089.66 1015.33600154616 2832095.017 1202088.69 1014.64312233339 2832094.719 1202089.625 1015.30000766326</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.741 1202089.66 1015.33600154616 2832098.936 1202096.351 1015.33602834224 2832095.017 1202088.69 1014.64312233339 2832094.741 1202089.66 1015.33600154616</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832099.261 1202096.869 1015.31103475377 2832096.477 1202098.218 1017.91457033327 2832100.341 1202097.179 1014.64315627141 2832099.261 1202096.869 1015.31103475377</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832100.341 1202097.179 1014.64315627141 2832098.936 1202096.351 1015.33602834224 2832099.261 1202096.869 1015.31103475377 2832100.341 1202097.179 1014.64315627141</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832099.261 1202096.869 1015.31103475377 2832099.261 1202096.869 994.229699699883 2832096.477 1202098.218 1017.91457033327 2832099.261 1202096.869 1015.31103475377</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.477 1202098.218 1017.91457033327 2832099.261 1202096.869 994.229699699883 2832093.324 1202099.746 1015.27301631263 2832096.477 1202098.218 1017.91457033327</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832099.261 1202096.869 994.229699699883 2832093.324 1202099.746 994.229675485848 2832093.324 1202099.746 1015.27301631263 2832099.261 1202096.869 994.229699699883</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832093.324 1202099.746 1015.27301631263 2832093.324 1202099.746 994.229675485848 2832093.099 1202099.21 1015.32300598653 2832093.324 1202099.746 1015.27301631263</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832093.099 1202099.21 1015.32300598653 2832089.914 1202091.611 994.229650898128 2832089.924 1202091.634 1015.3219830318 2832093.099 1202099.21 1015.32300598653</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832093.324 1202099.746 994.229675485848 2832089.914 1202091.611 994.229650898128 2832093.099 1202099.21 1015.32300598653 2832093.324 1202099.746 994.229675485848</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832089.924 1202091.634 1015.3219830318 2832089.914 1202091.611 994.229650898128 2832089.914 1202091.611 1015.29998678478 2832089.924 1202091.634 1015.3219830318</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832089.914 1202091.611 1015.29998678478 2832089.914 1202091.611 994.229650898128 2832094.719 1202089.625 1015.30000766326 2832089.914 1202091.611 1015.29998678478</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832089.914 1202091.611 994.229650898128 2832094.719 1202089.625 994.229671105267 2832094.719 1202089.625 1015.30000766326 2832089.914 1202091.611 994.229650898128</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832098.936 1202096.351 1015.33602834224 2832099.261 1202096.869 994.229699699883 2832099.261 1202096.869 1015.31103475377 2832098.936 1202096.351 1015.33602834224</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.719 1202089.625 1015.30000766326 2832094.719 1202089.625 994.229671105267 2832094.741 1202089.66 1015.33600154616 2832094.719 1202089.625 1015.30000766326</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.741 1202089.66 1015.33600154616 2832094.719 1202089.625 994.229671105267 2832098.936 1202096.351 1015.33602834224 2832094.741 1202089.66 1015.33600154616</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832098.936 1202096.351 1015.33602834224 2832094.719 1202089.625 994.229671105267 2832099.261 1202096.869 994.229699699883 2832098.936 1202096.351 1015.33602834224</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832099.261 1202096.869 994.229699699883 2832094.719 1202089.625 994.229671105267 2832093.324 1202099.746 994.229675485848 2832099.261 1202096.869 994.229699699883</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832093.324 1202099.746 994.229675485848 2832094.719 1202089.625 994.229671105267 2832089.914 1202091.611 994.229650898128 2832093.324 1202099.746 994.229675485848</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_BFD66DF5-991D-4EAC-8D0C-C5DFBAB58887">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>BFD66DF5-991D-4EAC-8D0C-C5DFBAB58887</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832005.075 1201582.575 1090.59710209006 2831997.951 1201581.339 1090.5970488029 2832004.855 1201580.578 1090.59710061414 2832005.075 1201582.575 1090.59710209006</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832005.654 1201587.823 1090.59710601812 2831998.348 1201588.628 1090.59705135106 2832005.075 1201582.575 1090.59710209006 2832005.654 1201587.823 1090.59710601812</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831998.348 1201588.628 1090.59705135106 2831997.951 1201581.339 1090.5970488029 2832005.075 1201582.575 1090.59710209006 2831998.348 1201588.628 1090.59705135106</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831994.15 1201589.091 1090.59701996521 2831994.062 1201588.292 1090.59701933667 2831998.348 1201588.628 1090.59705135106 2831994.15 1201589.091 1090.59701996521</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831994.062 1201588.292 1090.59701933667 2831997.951 1201581.339 1090.5970488029 2831998.348 1201588.628 1090.59705135106 2831994.062 1201588.292 1090.59701933667</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831993.351 1201581.846 1090.59701431255 2831997.951 1201581.339 1090.5970488029 2831994.062 1201588.292 1090.59701933667 2831993.351 1201581.846 1090.59701431255</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831998.348 1201588.628 1090.59705135106 2831994.15 1201589.091 1084.08415163199 2831994.15 1201589.091 1090.59701996521 2831998.348 1201588.628 1090.59705135106</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831998.348 1201588.628 1084.08418278712 2831994.15 1201589.091 1084.08415163199 2831998.348 1201588.628 1090.59705135106 2831998.348 1201588.628 1084.08418278712</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831994.15 1201589.091 1090.59701996521 2831994.062 1201588.292 1084.08415099638 2831994.062 1201588.292 1090.59701933667 2831994.15 1201589.091 1090.59701996521</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831994.15 1201589.091 1084.08415163199 2831994.062 1201588.292 1084.08415099638 2831994.15 1201589.091 1090.59701996521 2831994.15 1201589.091 1084.08415163199</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831994.062 1201588.292 1090.59701933667 2831993.351 1201581.846 1084.08414591493 2831993.351 1201581.846 1090.59701431255 2831994.062 1201588.292 1090.59701933667</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831994.062 1201588.292 1084.08415099638 2831993.351 1201581.846 1084.08414591493 2831994.062 1201588.292 1090.59701933667 2831994.062 1201588.292 1084.08415099638</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831993.351 1201581.846 1090.59701431255 2831997.951 1201581.339 1084.08418015188 2831997.951 1201581.339 1090.5970488029 2831993.351 1201581.846 1090.59701431255</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831993.351 1201581.846 1084.08414591493 2831997.951 1201581.339 1084.08418015188 2831993.351 1201581.846 1090.59701431255 2831993.351 1201581.846 1084.08414591493</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831997.951 1201581.339 1090.5970488029 2832004.855 1201580.578 1084.08423158252 2832004.855 1201580.578 1090.59710061414 2831997.951 1201581.339 1090.5970488029</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831997.951 1201581.339 1084.08418015188 2832004.855 1201580.578 1084.08423158252 2831997.951 1201581.339 1090.5970488029 2831997.951 1201581.339 1084.08418015188</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832004.855 1201580.578 1090.59710061414 2832005.075 1201582.575 1084.08423307669 2832005.075 1201582.575 1090.59710209006 2832004.855 1201580.578 1090.59710061414</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832004.855 1201580.578 1084.08423158252 2832005.075 1201582.575 1084.08423307669 2832004.855 1201580.578 1090.59710061414 2832004.855 1201580.578 1084.08423158252</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832005.075 1201582.575 1090.59710209006 2832005.654 1201587.823 1084.08423705239 2832005.654 1201587.823 1090.59710601812 2832005.075 1201582.575 1090.59710209006</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832005.075 1201582.575 1084.08423307669 2832005.654 1201587.823 1084.08423705239 2832005.075 1201582.575 1090.59710209006 2832005.075 1201582.575 1084.08423307669</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832005.654 1201587.823 1090.59710601812 2832005.654 1201587.823 1084.08423705239 2831998.348 1201588.628 1090.59705135106 2832005.654 1201587.823 1090.59710601812</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831998.348 1201588.628 1090.59705135106 2832005.654 1201587.823 1084.08423705239 2831998.348 1201588.628 1084.08418278712 2831998.348 1201588.628 1090.59705135106</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831998.348 1201588.628 1084.08418278712 2831994.062 1201588.292 1084.08415099638 2831994.15 1201589.091 1084.08415163199 2831998.348 1201588.628 1084.08418278712</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831998.348 1201588.628 1084.08418278712 2831997.951 1201581.339 1084.08418015188 2831994.062 1201588.292 1084.08415099638 2831998.348 1201588.628 1084.08418278712</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831998.348 1201588.628 1084.08418278712 2832005.075 1201582.575 1084.08423307669 2831997.951 1201581.339 1084.08418015188 2831998.348 1201588.628 1084.08418278712</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831998.348 1201588.628 1084.08418278712 2832005.654 1201587.823 1084.08423705239 2832005.075 1201582.575 1084.08423307669 2831998.348 1201588.628 1084.08418278712</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831994.062 1201588.292 1084.08415099638 2831997.951 1201581.339 1084.08418015188 2831993.351 1201581.846 1084.08414591493 2831994.062 1201588.292 1084.08415099638</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831997.951 1201581.339 1084.08418015188 2832005.075 1201582.575 1084.08423307669 2832004.855 1201580.578 1084.08423158252 2831997.951 1201581.339 1084.08418015188</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_587C6645-4584-4463-B39E-B650A80606D3">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Offenes Gebaeude</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>587C6645-4584-4463-B39E-B650A80606D3</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831770.454 1201914.339 1142.20857150606 2831765.049 1201924.98 1142.20854794889 2831763.032 1201916.005 1142.20851852844 2831770.454 1201914.339 1142.20857150606</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831772.47 1201923.314 1142.20860066403 2831765.049 1201924.98 1142.20854794889 2831770.454 1201914.339 1142.20857150606 2831772.47 1201923.314 1142.20860066403</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831763.032 1201916.005 1142.20851852844 2831770.454 1201914.339 1142.10858910407 2831770.454 1201914.339 1142.20857150606 2831763.032 1201916.005 1142.20851852844</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831763.032 1201916.005 1142.10853613235 2831770.454 1201914.339 1142.10858910407 2831763.032 1201916.005 1142.20851852844 2831763.032 1201916.005 1142.10853613235</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831770.454 1201914.339 1142.20857150606 2831772.47 1201923.314 1142.10861826102 2831772.47 1201923.314 1142.20860066403 2831770.454 1201914.339 1142.20857150606</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831770.454 1201914.339 1142.10858910407 2831772.47 1201923.314 1142.10861826102 2831770.454 1201914.339 1142.20857150606 2831770.454 1201914.339 1142.10858910407</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831772.47 1201923.314 1142.20860066403 2831765.049 1201924.98 1142.10856555177 2831765.049 1201924.98 1142.20854794889 2831772.47 1201923.314 1142.20860066403</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831772.47 1201923.314 1142.10861826102 2831765.049 1201924.98 1142.10856555177 2831772.47 1201923.314 1142.20860066403 2831772.47 1201923.314 1142.10861826102</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831765.049 1201924.98 1142.20854794889 2831765.049 1201924.98 1142.10856555177 2831763.032 1201916.005 1142.20851852844 2831765.049 1201924.98 1142.20854794889</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831763.032 1201916.005 1142.20851852844 2831765.049 1201924.98 1142.10856555177 2831763.032 1201916.005 1142.10853613235 2831763.032 1201916.005 1142.20851852844</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831763.032 1201916.005 1142.10853613235 2831765.049 1201924.98 1142.10856555177 2831770.454 1201914.339 1142.10858910407 2831763.032 1201916.005 1142.10853613235</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831770.454 1201914.339 1142.10858910407 2831765.049 1201924.98 1142.10856555177 2831772.47 1201923.314 1142.10861826102 2831770.454 1201914.339 1142.10858910407</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_F5D9DCCE-EC58-4664-9074-E49FE763C2E7">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>F5D9DCCE-EC58-4664-9074-E49FE763C2E7</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832026.634 1201588.878 1096.37825991926 2832039.82 1201590.776 1098.86492777864 2832036.441 1201595.661 1096.3783326789 2832026.634 1201588.878 1096.37825991926</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832030.011 1201583.992 1098.86485484277 2832039.82 1201590.776 1098.86492777864 2832026.634 1201588.878 1096.37825991926 2832030.011 1201583.992 1098.86485484277</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.39 1201579.108 1096.37831256194 2832039.82 1201590.776 1098.86492777864 2832030.011 1201583.992 1098.86485484277 2832033.39 1201579.108 1096.37831256194</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832043.199 1201585.891 1096.37838534221 2832039.82 1201590.776 1098.86492777864 2832033.39 1201579.108 1096.37831256194 2832043.199 1201585.891 1096.37838534221</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832036.441 1201595.661 1096.3783326789 2832036.315 1201594.965 1096.58829542801 2832026.634 1201588.878 1096.37825991926 2832036.441 1201595.661 1096.3783326789</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832039.82 1201590.776 1098.86492777864 2832036.315 1201594.965 1096.58829542801 2832036.441 1201595.661 1096.3783326789 2832039.82 1201590.776 1098.86492777864</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832026.634 1201588.878 1096.37825991926 2832027.329 1201588.751 1096.58722892023 2832030.011 1201583.992 1098.86485484277 2832026.634 1201588.878 1096.37825991926</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832036.315 1201594.965 1096.58829542801 2832027.329 1201588.751 1096.58722892023 2832026.634 1201588.878 1096.37825991926 2832036.315 1201594.965 1096.58829542801</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832030.011 1201583.992 1098.86485484277 2832033.516 1201579.804 1096.58827697095 2832033.39 1201579.108 1096.37831256194 2832030.011 1201583.992 1098.86485484277</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832027.329 1201588.751 1096.58722892023 2832030.423 1201584.277 1098.86485791139 2832030.011 1201583.992 1098.86485484277 2832027.329 1201588.751 1096.58722892023</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832030.423 1201584.277 1098.86485791139 2832033.516 1201579.804 1096.58827697095 2832030.011 1201583.992 1098.86485484277 2832030.423 1201584.277 1098.86485791139</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.39 1201579.108 1096.37831256194 2832042.503 1201586.017 1096.58734383796 2832043.199 1201585.891 1096.37838534221 2832033.39 1201579.108 1096.37831256194</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.39 1201579.108 1096.37831256194 2832033.516 1201579.804 1096.58827697095 2832042.503 1201586.017 1096.58734383796 2832033.39 1201579.108 1096.37831256194</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832043.199 1201585.891 1096.37838534221 2832042.503 1201586.017 1096.58734383796 2832039.82 1201590.776 1098.86492777864 2832043.199 1201585.891 1096.37838534221</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832042.503 1201586.017 1096.58734383796 2832039.409 1201590.492 1098.86492472789 2832039.82 1201590.776 1098.86492777864 2832042.503 1201586.017 1096.58734383796</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832039.409 1201590.492 1098.86492472789 2832036.315 1201594.965 1096.58829542801 2832039.82 1201590.776 1098.86492777864 2832039.409 1201590.492 1098.86492472789</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832042.503 1201586.017 1096.58734383796 2832042.503 1201586.017 1082.92271266737 2832039.409 1201590.492 1098.86492472789 2832042.503 1201586.017 1096.58734383796</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832039.409 1201590.492 1098.86492472789 2832036.315 1201594.965 1082.92266541497 2832036.315 1201594.965 1096.58829542801 2832039.409 1201590.492 1098.86492472789</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832042.503 1201586.017 1082.92271266737 2832036.315 1201594.965 1082.92266541497 2832039.409 1201590.492 1098.86492472789 2832042.503 1201586.017 1082.92271266737</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832036.315 1201594.965 1096.58829542801 2832036.315 1201594.965 1082.92266541497 2832027.329 1201588.751 1096.58722892023 2832036.315 1201594.965 1096.58829542801</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832036.315 1201594.965 1082.92266541497 2832027.329 1201588.751 1082.92259954033 2832027.329 1201588.751 1096.58722892023 2832036.315 1201594.965 1082.92266541497</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832027.329 1201588.751 1096.58722892023 2832027.329 1201588.751 1082.92259954033 2832030.423 1201584.277 1098.86485791139 2832027.329 1201588.751 1096.58722892023</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832030.423 1201584.277 1098.86485791139 2832033.516 1201579.804 1082.92264678018 2832033.516 1201579.804 1096.58827697095 2832030.423 1201584.277 1098.86485791139</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832027.329 1201588.751 1082.92259954033 2832033.516 1201579.804 1082.92264678018 2832030.423 1201584.277 1098.86485791139 2832027.329 1201588.751 1082.92259954033</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.516 1201579.804 1096.58827697095 2832033.516 1201579.804 1082.92264678018 2832033.703 1201579.933 1087.93577881334 2832033.516 1201579.804 1096.58827697095</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.516 1201579.804 1082.92264678018 2832033.703 1201579.933 1087.8357961516 2832033.703 1201579.933 1087.93577881334 2832033.516 1201579.804 1082.92264678018</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.516 1201579.804 1082.92264678018 2832037.77 1201582.745 1087.83582612425 2832033.703 1201579.933 1087.8357961516 2832033.516 1201579.804 1082.92264678018</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.77 1201582.745 1087.83582612425 2832033.516 1201579.804 1082.92264678018 2832042.503 1201586.017 1082.92271266737 2832037.77 1201582.745 1087.83582612425</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.703 1201579.933 1087.93577881334 2832035.716 1201581.325 1088.64667038969 2832033.516 1201579.804 1096.58827697095 2832033.703 1201579.933 1087.93577881334</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.516 1201579.804 1096.58827697095 2832035.716 1201581.325 1088.64667038969 2832036.991 1201582.206 1088.20575624311 2832033.516 1201579.804 1096.58827697095</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.77 1201582.745 1087.93580878867 2832033.516 1201579.804 1096.58827697095 2832036.991 1201582.206 1088.20575624311 2832037.77 1201582.745 1087.93580878867</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.516 1201579.804 1096.58827697095 2832037.77 1201582.745 1087.93580878867 2832042.503 1201586.017 1096.58734383796 2832033.516 1201579.804 1096.58827697095</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832042.503 1201586.017 1082.92271266737 2832042.503 1201586.017 1096.58734383796 2832037.77 1201582.745 1087.93580878867 2832042.503 1201586.017 1082.92271266737</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832036.937 1201582.169 1088.12476988739 2832035.716 1201581.325 1088.54668772662 2832033.703 1201579.933 1087.8357961516 2832036.937 1201582.169 1088.12476988739</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.77 1201582.745 1087.83582612425 2832036.937 1201582.169 1088.12476988739 2832033.703 1201579.933 1087.8357961516 2832037.77 1201582.745 1087.83582612425</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.77 1201582.745 1087.93580878867 2832037.77 1201582.745 1087.83582612425 2832042.503 1201586.017 1082.92271266737 2832037.77 1201582.745 1087.93580878867</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832042.503 1201586.017 1082.92271266737 2832033.516 1201579.804 1082.92264678018 2832036.315 1201594.965 1082.92266541497 2832042.503 1201586.017 1082.92271266737</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832036.315 1201594.965 1082.92266541497 2832033.516 1201579.804 1082.92264678018 2832027.329 1201588.751 1082.92259954033 2832036.315 1201594.965 1082.92266541497</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_7780DD45-6902-49C7-B9BC-BA6D130CF0FA">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Offenes Gebaeude</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>7780DD45-6902-49C7-B9BC-BA6D130CF0FA</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832034.425 1201580.432 1088.19073992658 2832035.617 1201577.164 1087.93579365936 2832037.631 1201578.558 1088.64668525948 2832034.425 1201580.432 1088.19073992658</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.703 1201579.933 1087.93577881334 2832035.617 1201577.164 1087.93579365936 2832034.425 1201580.432 1088.19073992658 2832033.703 1201579.933 1087.93577881334</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.631 1201578.558 1088.64668525948 2832035.716 1201581.325 1088.64667038969 2832034.425 1201580.432 1088.19073992658 2832037.631 1201578.558 1088.64668525948</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832039.685 1201579.977 1087.9358236435 2832037.77 1201582.745 1087.93580878867 2832037.034 1201582.236 1088.19075916033 2832039.685 1201579.977 1087.9358236435</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.034 1201582.236 1088.19075916033 2832037.631 1201578.558 1088.64668525948 2832039.685 1201579.977 1087.9358236435 2832037.034 1201582.236 1088.19075916033</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.034 1201582.236 1088.19075916033 2832035.716 1201581.325 1088.64667038969 2832037.631 1201578.558 1088.64668525948 2832037.034 1201582.236 1088.19075916033</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832035.617 1201577.164 1087.93579365936 2832037.631 1201578.558 1088.54670259417 2832037.631 1201578.558 1088.64668525948 2832035.617 1201577.164 1087.93579365936</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832035.617 1201577.164 1087.83581099538 2832037.631 1201578.558 1088.54670259417 2832035.617 1201577.164 1087.93579365936 2832035.617 1201577.164 1087.83581099538</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.631 1201578.558 1088.64668525948 2832037.631 1201578.558 1088.54670259417 2832039.685 1201579.977 1087.9358236435 2832037.631 1201578.558 1088.64668525948</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.631 1201578.558 1088.54670259417 2832039.685 1201579.977 1087.83584097684 2832039.685 1201579.977 1087.9358236435 2832037.631 1201578.558 1088.54670259417</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832039.685 1201579.977 1087.83584097684 2832037.77 1201582.745 1087.93580878867 2832039.685 1201579.977 1087.9358236435 2832039.685 1201579.977 1087.83584097684</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.77 1201582.745 1087.93580878867 2832039.685 1201579.977 1087.83584097684 2832037.77 1201582.745 1087.83582612425 2832037.77 1201582.745 1087.93580878867</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.703 1201579.933 1087.93577881334 2832035.617 1201577.164 1087.83581099538 2832035.617 1201577.164 1087.93579365936 2832033.703 1201579.933 1087.93577881334</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.703 1201579.933 1087.8357961516 2832035.617 1201577.164 1087.83581099538 2832033.703 1201579.933 1087.93577881334 2832033.703 1201579.933 1087.8357961516</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832033.703 1201579.933 1087.8357961516 2832034.425 1201580.432 1088.09075726436 2832035.617 1201577.164 1087.83581099538 2832033.703 1201579.933 1087.8357961516</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832034.425 1201580.432 1088.09075726436 2832035.716 1201581.325 1088.54668772662 2832037.631 1201578.558 1088.54670259417 2832034.425 1201580.432 1088.09075726436</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832034.425 1201580.432 1088.09075726436 2832037.631 1201578.558 1088.54670259417 2832035.617 1201577.164 1087.83581099538 2832034.425 1201580.432 1088.09075726436</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.631 1201578.558 1088.54670259417 2832037.034 1201582.236 1088.0907764964 2832039.685 1201579.977 1087.83584097684 2832037.631 1201578.558 1088.54670259417</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832037.034 1201582.236 1088.0907764964 2832037.77 1201582.745 1087.83582612425 2832039.685 1201579.977 1087.83584097684 2832037.034 1201582.236 1088.0907764964</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832035.716 1201581.325 1088.54668772662 2832037.034 1201582.236 1088.0907764964 2832037.631 1201578.558 1088.54670259417 2832035.716 1201581.325 1088.54668772662</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_11332822-7E4A-423D-8AA4-88F2DD00B35E">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>11332822-7E4A-423D-8AA4-88F2DD00B35E</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832045.233 1201584.814 1089.3576178364 2832047.593 1201596.445 1088.29181805444 2832041.551 1201594.034 1089.35758863868 2832045.233 1201584.814 1089.3576178364</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832051.274 1201587.226 1088.29184729481 2832047.593 1201596.445 1088.29181805444 2832045.233 1201584.814 1089.3576178364 2832051.274 1201587.226 1088.29184729481</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832041.551 1201594.034 1089.35758863868 2832045.511 1201585.464 1089.27563400143 2832045.233 1201584.814 1089.3576178364 2832041.551 1201594.034 1089.35758863868</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832047.593 1201596.445 1088.29181805444 2832042.201 1201593.755 1089.27560774984 2832041.551 1201594.034 1089.35758863868 2832047.593 1201596.445 1088.29181805444</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832042.201 1201593.755 1089.27560774984 2832045.511 1201585.464 1089.27563400143 2832041.551 1201594.034 1089.35758863868 2832042.201 1201593.755 1089.27560774984</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832045.233 1201584.814 1089.3576178364 2832050.624 1201587.505 1088.37382816673 2832051.274 1201587.226 1088.29184729481 2832045.233 1201584.814 1089.3576178364</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832045.233 1201584.814 1089.3576178364 2832045.511 1201585.464 1089.27563400143 2832050.624 1201587.505 1088.37382816673 2832045.233 1201584.814 1089.3576178364</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832051.274 1201587.226 1088.29184729481 2832047.314 1201595.795 1088.37380187701 2832047.593 1201596.445 1088.29181805444 2832051.274 1201587.226 1088.29184729481</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832050.624 1201587.505 1088.37382816673 2832047.314 1201595.795 1088.37380187701 2832051.274 1201587.226 1088.29184729481 2832050.624 1201587.505 1088.37382816673</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832047.314 1201595.795 1088.37380187701 2832042.201 1201593.755 1089.27560774984 2832047.593 1201596.445 1088.29181805444 2832047.314 1201595.795 1088.37380187701</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832050.624 1201587.505 1088.37382816673 2832050.624 1201587.505 1078.8194838549 2832047.314 1201595.795 1088.37380187701 2832050.624 1201587.505 1088.37382816673</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832050.624 1201587.505 1078.8194838549 2832047.314 1201595.795 1078.81945801489 2832047.314 1201595.795 1088.37380187701 2832050.624 1201587.505 1078.8194838549</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832047.314 1201595.795 1088.37380187701 2832042.201 1201593.755 1078.81942059924 2832042.201 1201593.755 1089.27560774984 2832047.314 1201595.795 1088.37380187701</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832047.314 1201595.795 1078.81945801489 2832042.201 1201593.755 1078.81942059924 2832047.314 1201595.795 1088.37380187701 2832047.314 1201595.795 1078.81945801489</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832042.201 1201593.755 1089.27560774984 2832045.511 1201585.464 1078.81944635934 2832045.511 1201585.464 1089.27563400143 2832042.201 1201593.755 1089.27560774984</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832042.201 1201593.755 1078.81942059924 2832045.511 1201585.464 1078.81944635934 2832042.201 1201593.755 1089.27560774984 2832042.201 1201593.755 1078.81942059924</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832045.511 1201585.464 1089.27563400143 2832045.511 1201585.464 1078.81944635934 2832050.624 1201587.505 1088.37382816673 2832045.511 1201585.464 1089.27563400143</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832050.624 1201587.505 1088.37382816673 2832045.511 1201585.464 1078.81944635934 2832050.624 1201587.505 1078.8194838549 2832050.624 1201587.505 1088.37382816673</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832050.624 1201587.505 1078.8194838549 2832045.511 1201585.464 1078.81944635934 2832047.314 1201595.795 1078.81945801489 2832050.624 1201587.505 1078.8194838549</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832047.314 1201595.795 1078.81945801489 2832045.511 1201585.464 1078.81944635934 2832042.201 1201593.755 1078.81942059924 2832047.314 1201595.795 1078.81945801489</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_7509619D-5819-4CCC-B1D0-A5E2C117909C">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Offenes Gebaeude</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>7509619D-5819-4CCC-B1D0-A5E2C117909C</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832134.761 1202070.376 1015.37916621818 2832140.166 1202081.324 1015.37920013742 2832129.378 1202073.035 1012.37166519048 2832134.761 1202070.376 1015.37916621818</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832140.166 1202081.324 1015.37920013742 2832134.784 1202083.981 1012.37169945831 2832129.378 1202073.035 1012.37166519048 2832140.166 1202081.324 1015.37920013742</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832140.145 1202067.719 1012.37171147851 2832140.166 1202081.324 1015.37920013742 2832134.761 1202070.376 1015.37916621818 2832140.145 1202067.719 1012.37171147851</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832145.55 1202078.665 1012.37174494589 2832140.166 1202081.324 1015.37920013742 2832140.145 1202067.719 1012.37171147851 2832145.55 1202078.665 1012.37174494589</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832129.378 1202073.035 1012.37166519048 2832134.761 1202070.376 1015.2791835749 2832134.761 1202070.376 1015.37916621818 2832129.378 1202073.035 1012.37166519048</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832129.378 1202073.035 1012.27168255086 2832134.761 1202070.376 1015.2791835749 2832129.378 1202073.035 1012.37166519048 2832129.378 1202073.035 1012.27168255086</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832134.761 1202070.376 1015.37916621818 2832134.761 1202070.376 1015.2791835749 2832140.145 1202067.719 1012.37171147851 2832134.761 1202070.376 1015.37916621818</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832134.761 1202070.376 1015.2791835749 2832140.145 1202067.719 1012.27172883157 2832140.145 1202067.719 1012.37171147851 2832134.761 1202070.376 1015.2791835749</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832140.145 1202067.719 1012.37171147851 2832145.55 1202078.665 1012.27176229726 2832145.55 1202078.665 1012.37174494589 2832140.145 1202067.719 1012.37171147851</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832140.145 1202067.719 1012.27172883157 2832145.55 1202078.665 1012.27176229726 2832140.145 1202067.719 1012.37171147851 2832140.145 1202067.719 1012.27172883157</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832145.55 1202078.665 1012.37174494589 2832140.166 1202081.324 1015.27921749241 2832140.166 1202081.324 1015.37920013742 2832145.55 1202078.665 1012.37174494589</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832145.55 1202078.665 1012.27176229726 2832140.166 1202081.324 1015.27921749241 2832145.55 1202078.665 1012.37174494589 2832145.55 1202078.665 1012.27176229726</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832140.166 1202081.324 1015.37920013742 2832140.166 1202081.324 1015.27921749241 2832134.784 1202083.981 1012.37169945831 2832140.166 1202081.324 1015.37920013742</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832140.166 1202081.324 1015.27921749241 2832134.784 1202083.981 1012.27171681691 2832134.784 1202083.981 1012.37169945831 2832140.166 1202081.324 1015.27921749241</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832134.784 1202083.981 1012.37169945831 2832134.784 1202083.981 1012.27171681691 2832129.378 1202073.035 1012.37166519048 2832134.784 1202083.981 1012.37169945831</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832129.378 1202073.035 1012.37166519048 2832134.784 1202083.981 1012.27171681691 2832129.378 1202073.035 1012.27168255086 2832129.378 1202073.035 1012.37166519048</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832129.378 1202073.035 1012.27168255086 2832140.166 1202081.324 1015.27921749241 2832134.761 1202070.376 1015.2791835749 2832129.378 1202073.035 1012.27168255086</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832129.378 1202073.035 1012.27168255086 2832134.784 1202083.981 1012.27171681691 2832140.166 1202081.324 1015.27921749241 2832129.378 1202073.035 1012.27168255086</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832134.761 1202070.376 1015.2791835749 2832140.166 1202081.324 1015.27921749241 2832140.145 1202067.719 1012.27172883157 2832134.761 1202070.376 1015.2791835749</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832140.145 1202067.719 1012.27172883157 2832140.166 1202081.324 1015.27921749241 2832145.55 1202078.665 1012.27176229726 2832140.145 1202067.719 1012.27172883157</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_760410DF-1738-4383-819C-79F95B29616B">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Offenes Gebaeude</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>760410DF-1738-4383-819C-79F95B29616B</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831965.799 1201625.781 1094.50713103262 2831970.23 1201625.329 1093.17439571891 2831968.198 1201627.775 1093.17438103521 2831965.799 1201625.781 1094.50713103262</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831970.23 1201625.329 1093.17439571891 2831965.799 1201625.781 1094.50713103262 2831967.831 1201623.336 1094.50714575569 2831970.23 1201625.329 1093.17439571891</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.796 1201622.475 1093.93123821563 2831967.831 1201623.336 1094.50714575569 2831965.799 1201625.781 1094.50713103262 2831966.796 1201622.475 1093.93123821563</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1093.81725650699 2831966.796 1201622.475 1093.93123821563 2831965.799 1201625.781 1094.50713103262 2831966.59 1201622.304 1093.81725650699</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1093.81725650699 2831965.799 1201625.781 1094.50713103262 2831963.4 1201623.788 1093.17434500525 2831966.59 1201622.304 1093.81725650699</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831963.4 1201623.788 1093.17434500525 2831968.108 1201618.124 1093.17437914042 2831966.994 1201621.818 1093.81725943744 2831963.4 1201623.788 1093.17434500525</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831968.108 1201618.124 1093.17437914042 2831969.265 1201619.086 1093.8172759418 2831966.994 1201621.818 1093.81725943744 2831968.108 1201618.124 1093.17437914042</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.994 1201621.818 1093.81725943744 2831966.59 1201622.304 1093.81725650699 2831963.4 1201623.788 1093.17434500525 2831966.994 1201621.818 1093.81725943744</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.265 1201619.086 1093.71729334146 2831969.265 1201619.086 1093.8172759418 2831968.108 1201618.124 1093.17437914042 2831969.265 1201619.086 1093.71729334146</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.265 1201619.086 1093.71729334146 2831968.108 1201618.124 1093.17437914042 2831968.108 1201618.124 1093.07439654081 2831969.265 1201619.086 1093.71729334146</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831970.23 1201625.329 1093.17439571891 2831968.198 1201627.775 1093.07439843754 2831968.198 1201627.775 1093.17438103521 2831970.23 1201625.329 1093.17439571891</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831970.23 1201625.329 1093.07441311909 2831968.198 1201627.775 1093.07439843754 2831970.23 1201625.329 1093.17439571891 2831970.23 1201625.329 1093.07441311909</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831968.198 1201627.775 1093.17438103521 2831965.799 1201625.781 1094.40714843648 2831965.799 1201625.781 1094.50713103262 2831968.198 1201627.775 1093.17438103521</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831968.198 1201627.775 1093.07439843754 2831965.799 1201625.781 1094.40714843648 2831968.198 1201627.775 1093.17438103521 2831968.198 1201627.775 1093.07439843754</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831965.799 1201625.781 1094.50713103262 2831965.799 1201625.781 1094.40714843648 2831963.4 1201623.788 1093.17434500525 2831965.799 1201625.781 1094.50713103262</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831965.799 1201625.781 1094.40714843648 2831963.4 1201623.788 1093.07436241065 2831963.4 1201623.788 1093.17434500525 2831965.799 1201625.781 1094.40714843648</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831963.4 1201623.788 1093.17434500525 2831968.108 1201618.124 1093.07439654081 2831968.108 1201618.124 1093.17437914042 2831963.4 1201623.788 1093.17434500525</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831963.4 1201623.788 1093.07436241065 2831968.108 1201618.124 1093.07439654081 2831963.4 1201623.788 1093.17434500525 2831963.4 1201623.788 1093.07436241065</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831965.799 1201625.781 1094.40714843648 2831970.23 1201625.329 1093.07441311909 2831967.831 1201623.336 1094.4071631574 2831965.799 1201625.781 1094.40714843648</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831970.23 1201625.329 1093.07441311909 2831965.799 1201625.781 1094.40714843648 2831968.198 1201627.775 1093.07439843754 2831970.23 1201625.329 1093.07441311909</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831965.799 1201625.781 1094.40714843648 2831966.796 1201622.475 1093.831255618 2831966.59 1201622.304 1093.71727390949 2831965.799 1201625.781 1094.40714843648</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831967.831 1201623.336 1094.4071631574 2831966.796 1201622.475 1093.831255618 2831965.799 1201625.781 1094.40714843648 2831967.831 1201623.336 1094.4071631574</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1093.71727390949 2831963.4 1201623.788 1093.07436241065 2831965.799 1201625.781 1094.40714843648 2831966.59 1201622.304 1093.71727390949</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831963.4 1201623.788 1093.07436241065 2831966.59 1201622.304 1093.71727390949 2831966.994 1201621.818 1093.71727683951 2831963.4 1201623.788 1093.07436241065</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831968.108 1201618.124 1093.07439654081 2831966.994 1201621.818 1093.71727683951 2831969.265 1201619.086 1093.71729334146 2831968.108 1201618.124 1093.07439654081</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831968.108 1201618.124 1093.07439654081 2831963.4 1201623.788 1093.07436241065 2831966.994 1201621.818 1093.71727683951 2831968.108 1201618.124 1093.07439654081</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_81143FD5-7C65-484D-9765-DAC10A9C4147">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>81143FD5-7C65-484D-9765-DAC10A9C4147</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.442 1201625.65 1096.81675602153 2831985.741 1201613.99 1094.06335500885 2831973.35 1201628.898 1094.06326456354 2831969.442 1201625.65 1096.81675602153</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831965.534 1201622.401 1094.06320588727 2831977.925 1201607.494 1094.06329652739 2831969.442 1201625.65 1096.81675602153 2831965.534 1201622.401 1094.06320588727</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831977.925 1201607.494 1094.06329652739 2831981.834 1201610.741 1096.81684693566 2831969.442 1201625.65 1096.81675602153 2831977.925 1201607.494 1094.06329652739</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.834 1201610.741 1096.81684693566 2831985.741 1201613.99 1094.06335500885 2831969.442 1201625.65 1096.81675602153 2831981.834 1201610.741 1096.81684693566</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831973.35 1201628.898 1094.06326456354 2831973.253 1201627.841 1094.47019289951 2831969.442 1201625.65 1096.81675602153 2831973.35 1201628.898 1094.06326456354</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831985.741 1201613.99 1094.06335500885 2831973.253 1201627.841 1094.47019289951 2831973.35 1201628.898 1094.06326456354 2831985.741 1201613.99 1094.06335500885</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.442 1201625.65 1096.81675602153 2831966.59 1201622.304 1094.46914304269 2831965.534 1201622.401 1094.06320588727 2831969.442 1201625.65 1096.81675602153</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831973.253 1201627.841 1094.47019289951 2831969.922 1201625.073 1096.81675951296 2831969.442 1201625.65 1096.81675602153 2831973.253 1201627.841 1094.47019289951</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.922 1201625.073 1096.81675951296 2831966.59 1201622.304 1094.46914304269 2831969.442 1201625.65 1096.81675602153 2831969.922 1201625.073 1096.81675951296</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831965.534 1201622.401 1094.06320588727 2831966.59 1201622.304 1094.46914304269 2831977.925 1201607.494 1094.06329652739 2831965.534 1201622.401 1094.06320588727</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831977.925 1201607.494 1094.06329652739 2831978.023 1201608.55 1094.47022653717 2831981.834 1201610.741 1096.81684693566 2831977.925 1201607.494 1094.06329652739</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1094.46914304269 2831978.023 1201608.55 1094.47022653717 2831977.925 1201607.494 1094.06329652739 2831966.59 1201622.304 1094.46914304269</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.834 1201610.741 1096.81684693566 2831984.685 1201614.087 1094.47027640745 2831985.741 1201613.99 1094.06335500885 2831981.834 1201610.741 1096.81684693566</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1096.73285711939 2831981.357 1201611.315 1096.81684340586 2831981.834 1201610.741 1096.81684693566 2831981.238 1201611.216 1096.73285711939</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.834 1201610.741 1096.81684693566 2831978.023 1201608.55 1094.47022653717 2831981.238 1201611.216 1096.73285711939 2831981.834 1201610.741 1096.81684693566</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.357 1201611.315 1096.81684340586 2831984.685 1201614.087 1094.47027640745 2831981.834 1201610.741 1096.81684693566 2831981.357 1201611.315 1096.81684340586</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831984.685 1201614.087 1094.47027640745 2831973.253 1201627.841 1094.47019289951 2831985.741 1201613.99 1094.06335500885 2831984.685 1201614.087 1094.47027640745</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.357 1201611.315 1096.81684340586 2831981.238 1201611.216 1096.73285711939 2831984.685 1201614.087 1094.47027640745 2831981.357 1201611.315 1096.81684340586</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1096.73285711939 2831981.238 1201611.216 1091.3667903472 2831984.685 1201614.087 1094.47027640745 2831981.238 1201611.216 1096.73285711939</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831983.315 1201612.946 1091.3668058505 2831984.685 1201614.087 1094.47027640745 2831981.238 1201611.216 1091.3667903472 2831983.315 1201612.946 1091.3668058505</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831984.685 1201614.087 1082.20940846203 2831984.685 1201614.087 1094.47027640745 2831983.315 1201612.946 1091.3668058505 2831984.685 1201614.087 1082.20940846203</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831983.315 1201612.946 1091.3668058505 2831983.315 1201612.946 1082.20939832065 2831984.685 1201614.087 1082.20940846203 2831983.315 1201612.946 1091.3668058505</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831984.685 1201614.087 1094.47027640745 2831973.253 1201627.841 1082.20932644849 2831973.253 1201627.841 1094.47019289951 2831984.685 1201614.087 1094.47027640745</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831984.685 1201614.087 1082.20940846203 2831973.253 1201627.841 1082.20932644849 2831984.685 1201614.087 1094.47027640745 2831984.685 1201614.087 1082.20940846203</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831970.23 1201625.329 1093.07441311909 2831973.253 1201627.841 1094.47019289951 2831973.253 1201627.841 1082.20932644849 2831970.23 1201625.329 1093.07441311909</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831973.253 1201627.841 1094.47019289951 2831970.23 1201625.329 1093.17439571891 2831969.922 1201625.073 1096.81675951296 2831973.253 1201627.841 1094.47019289951</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831973.253 1201627.841 1094.47019289951 2831970.23 1201625.329 1093.07441311909 2831970.23 1201625.329 1093.17439571891 2831973.253 1201627.841 1094.47019289951</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831970.23 1201625.329 1093.07441311909 2831973.253 1201627.841 1082.20932644849 2831966.59 1201622.304 1082.20927693734 2831970.23 1201625.329 1093.07441311909</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831967.831 1201623.336 1094.50714575569 2831969.922 1201625.073 1096.81675951296 2831970.23 1201625.329 1093.17439571891 2831967.831 1201623.336 1094.50714575569</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1094.46914304269 2831969.922 1201625.073 1096.81675951296 2831967.831 1201623.336 1094.50714575569 2831966.59 1201622.304 1094.46914304269</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1082.20927693734 2831966.59 1201622.304 1093.71727390949 2831970.23 1201625.329 1093.07441311909 2831966.59 1201622.304 1082.20927693734</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.904 1201622.565 1093.89224581384 2831967.831 1201623.336 1094.4071631574 2831970.23 1201625.329 1093.07441311909 2831966.904 1201622.565 1093.89224581384</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1093.71727390949 2831966.904 1201622.565 1093.89224581384 2831970.23 1201625.329 1093.07441311909 2831966.59 1201622.304 1093.71727390949</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831967.831 1201623.336 1094.50714575569 2831966.862 1201622.53 1093.96823227257 2831966.59 1201622.304 1094.46914304269 2831967.831 1201623.336 1094.50714575569</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1093.81725650699 2831966.59 1201622.304 1094.46914304269 2831966.862 1201622.53 1093.96823227257 2831966.59 1201622.304 1093.81725650699</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1094.46914304269 2831967.198 1201621.572 1093.81726091774 2831969.265 1201619.086 1093.8172759418 2831966.59 1201622.304 1094.46914304269</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831966.59 1201622.304 1093.81725650699 2831967.198 1201621.572 1093.81726091774 2831966.59 1201622.304 1094.46914304269 2831966.59 1201622.304 1093.81725650699</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.265 1201619.086 1093.8172759418 2831978.023 1201608.55 1094.47022653717 2831966.59 1201622.304 1094.46914304269 2831969.265 1201619.086 1093.8172759418</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.265 1201619.086 1093.8172759418 2831969.265 1201619.086 1093.71729334146 2831978.023 1201608.55 1094.47022653717 2831969.265 1201619.086 1093.8172759418</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831978.023 1201608.55 1082.20935910929 2831969.265 1201619.086 1093.71729334146 2831966.59 1201622.304 1082.20927693734 2831978.023 1201608.55 1082.20935910929</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.265 1201619.086 1093.71729334146 2831978.023 1201608.55 1082.20935910929 2831978.023 1201608.55 1094.47022653717 2831969.265 1201619.086 1093.71729334146</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831969.265 1201619.086 1093.71729334146 2831967.291 1201621.46 1093.7172789945 2831966.59 1201622.304 1082.20927693734 2831969.265 1201619.086 1093.71729334146</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831967.291 1201621.46 1093.7172789945 2831966.59 1201622.304 1093.71727390949 2831966.59 1201622.304 1082.20927693734 2831967.291 1201621.46 1093.7172789945</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1091.3667903472 2831981.238 1201611.216 1096.73285711939 2831978.023 1201608.55 1094.47022653717 2831981.238 1201611.216 1091.3667903472</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831978.16 1201608.664 1091.36676735581 2831981.238 1201611.216 1091.3667903472 2831978.023 1201608.55 1094.47022653717 2831978.16 1201608.664 1091.36676735581</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831978.16 1201608.664 1091.36676735581 2831978.023 1201608.55 1094.47022653717 2831978.023 1201608.55 1082.20935910929 2831978.16 1201608.664 1091.36676735581</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831978.023 1201608.55 1082.20935910929 2831978.16 1201608.664 1082.20936012516 2831978.16 1201608.664 1091.36676735581 2831978.023 1201608.55 1082.20935910929</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1082.20938293775 2831973.253 1201627.841 1082.20932644849 2831984.685 1201614.087 1082.20940846203 2831981.238 1201611.216 1082.20938293775</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1082.20938293775 2831966.59 1201622.304 1082.20927693734 2831973.253 1201627.841 1082.20932644849 2831981.238 1201611.216 1082.20938293775</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1082.20938293775 2831978.023 1201608.55 1082.20935910929 2831966.59 1201622.304 1082.20927693734 2831981.238 1201611.216 1082.20938293775</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_75C911D0-16A9-47AA-BA9B-737A8BF9A57B">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151008</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>75C911D0-16A9-47AA-BA9B-737A8BF9A57B</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831986.359 1201609.284 1091.3668282291 2831981.238 1201611.216 1091.3667903472 2831981.205 1201605 1091.3667897706 2831986.359 1201609.284 1091.3668282291</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1091.3667903472 2831978.16 1201608.664 1091.36676735581 2831981.205 1201605 1091.3667897706 2831981.238 1201611.216 1091.3667903472</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1091.3667903472 2831986.359 1201609.284 1091.3668282291 2831983.315 1201612.946 1091.3668058505 2831981.238 1201611.216 1091.3667903472</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831986.359 1201609.284 1082.20942039989 2831983.315 1201612.946 1091.3668058505 2831986.359 1201609.284 1091.3668282291 2831986.359 1201609.284 1082.20942039989</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831986.359 1201609.284 1082.20942039989 2831983.315 1201612.946 1082.20939832065 2831983.315 1201612.946 1091.3668058505 2831986.359 1201609.284 1082.20942039989</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831978.16 1201608.664 1091.36676735581 2831981.205 1201605 1082.20938224018 2831981.205 1201605 1091.3667897706 2831978.16 1201608.664 1091.36676735581</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831978.16 1201608.664 1082.20936012516 2831981.205 1201605 1082.20938224018 2831978.16 1201608.664 1091.36676735581 2831978.16 1201608.664 1082.20936012516</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.205 1201605 1091.3667897706 2831986.359 1201609.284 1082.20942039989 2831986.359 1201609.284 1091.3668282291 2831981.205 1201605 1091.3667897706</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.205 1201605 1082.20938224018 2831986.359 1201609.284 1082.20942039989 2831981.205 1201605 1091.3667897706 2831981.205 1201605 1082.20938224018</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831981.238 1201611.216 1082.20938293775 2831981.205 1201605 1082.20938224018 2831978.16 1201608.664 1082.20936012516 2831981.238 1201611.216 1082.20938293775</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831986.359 1201609.284 1082.20942039989 2831981.205 1201605 1082.20938224018 2831981.238 1201611.216 1082.20938293775 2831986.359 1201609.284 1082.20942039989</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2831986.359 1201609.284 1082.20942039989 2831981.238 1201611.216 1082.20938293775 2831983.315 1201612.946 1082.20939832065 2831986.359 1201609.284 1082.20942039989</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_7954F4E9-D0DA-4AA1-855A-3A3CB55F2489">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20151016</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151016</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Bruecke gedeckt</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>7954F4E9-D0DA-4AA1-855A-3A3CB55F2489</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.53 1202084.382 998.877861274904 2832080.6 1202073.249 997.933942844701 2832096.261 1202083.235 997.93402761049 2832095.53 1202084.382 998.877861274904</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.6 1202073.249 997.933942844701 2832095.53 1202084.382 998.877861274904 2832079.757 1202074.325 998.877775792231 2832080.6 1202073.249 997.933942844701</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.951 1202083.082 997.587091002107 2832095.951 1202084.65 998.877863526362 2832096.261 1202083.235 997.93402761049 2832096.951 1202083.082 997.587091002107</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.951 1202084.65 998.877863526362 2832095.53 1202084.382 998.877861274904 2832096.261 1202083.235 997.93402761049 2832095.951 1202084.65 998.877863526362</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.261 1202083.235 997.93402761049 2832080.91 1202072.854 997.587004250806 2832096.951 1202083.082 997.587091002107 2832096.261 1202083.235 997.93402761049</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.261 1202083.235 997.93402761049 2832080.6 1202073.249 997.933942844701 2832080.91 1202072.854 997.587004250806 2832096.261 1202083.235 997.93402761049</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.53 1202084.382 998.877861274904 2832079.029 1202075.254 998.009924267744 2832079.757 1202074.325 998.877775792231 2832095.53 1202084.382 998.877861274904</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.53 1202084.382 998.877861274904 2832094.859 1202085.435 997.956019490433 2832079.029 1202075.254 998.009924267744 2832095.53 1202084.382 998.877861274904</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.859 1202085.435 997.956019490433 2832078.984 1202075.313 997.95593350793 2832079.029 1202075.254 998.009924267744 2832094.859 1202085.435 997.956019490433</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.011 1202086.125 997.587085063556 2832094.859 1202085.435 997.956019490433 2832095.951 1202084.65 998.877863526362 2832095.011 1202086.125 997.587085063556</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.966 1202075.336 997.933937272937 2832094.859 1202085.435 997.956019490433 2832095.011 1202086.125 997.587085063556 2832078.966 1202075.336 997.933937272937</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.674 1202075.708 997.586996629996 2832078.966 1202075.336 997.933937272937 2832095.011 1202086.125 997.587085063556 2832078.674 1202075.708 997.586996629996</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.951 1202084.65 998.877863526362 2832094.859 1202085.435 997.956019490433 2832095.53 1202084.382 998.877861274904 2832095.951 1202084.65 998.877863526362</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.984 1202075.313 997.95593350793 2832094.859 1202085.435 997.956019490433 2832078.966 1202075.336 997.933937272937 2832078.984 1202075.313 997.95593350793</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.951 1202083.082 997.587091002107 2832096.261 1202083.235 997.93402761049 2832095.951 1202084.65 998.877863526362 2832096.951 1202083.082 997.587091002107</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.261 1202083.235 997.93402761049 2832095.53 1202084.382 998.877861274904 2832095.951 1202084.65 998.877863526362 2832096.261 1202083.235 997.93402761049</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.91 1202072.854 997.587004250806 2832096.261 1202083.235 997.93402761049 2832096.951 1202083.082 997.587091002107 2832080.91 1202072.854 997.587004250806</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.6 1202073.249 997.933942844701 2832096.261 1202083.235 997.93402761049 2832080.91 1202072.854 997.587004250806 2832080.6 1202073.249 997.933942844701</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.859 1202085.435 997.956019490433 2832095.011 1202086.125 997.587085063556 2832095.951 1202084.65 998.877863526362 2832094.859 1202085.435 997.956019490433</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.859 1202085.435 997.956019490433 2832095.951 1202084.65 998.877863526362 2832095.53 1202084.382 998.877861274904 2832094.859 1202085.435 997.956019490433</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.011 1202086.125 997.587085063556 2832094.859 1202085.435 997.956019490433 2832078.966 1202075.336 997.933937272937 2832095.011 1202086.125 997.587085063556</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.859 1202085.435 997.956019490433 2832078.984 1202075.313 997.95593350793 2832078.966 1202075.336 997.933937272937 2832094.859 1202085.435 997.956019490433</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.011 1202086.125 997.587085063556 2832078.966 1202075.336 997.933937272937 2832078.674 1202075.708 997.586996629996 2832095.011 1202086.125 997.587085063556</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.261 1202083.235 997.93402761049 2832094.859 1202085.435 994.483623206444 2832095.53 1202084.382 998.877861274904 2832096.261 1202083.235 997.93402761049</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832096.261 1202083.235 994.483627463356 2832094.859 1202085.435 994.483623206444 2832096.261 1202083.235 997.93402761049 2832096.261 1202083.235 994.483627463356</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832095.53 1202084.382 998.877861274904 2832094.859 1202085.435 994.483623206444 2832094.859 1202085.435 997.956019490433 2832095.53 1202084.382 998.877861274904</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832094.859 1202085.435 994.483623206444 2832078.984 1202075.313 997.95593350793 2832094.859 1202085.435 997.956019490433 2832094.859 1202085.435 994.483623206444</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.984 1202075.313 997.95593350793 2832094.859 1202085.435 994.483623206444 2832078.984 1202075.313 994.483537526234 2832078.984 1202075.313 997.95593350793</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.6 1202073.249 997.933942844701 2832096.261 1202083.235 994.483627463356 2832096.261 1202083.235 997.93402761049 2832080.6 1202073.249 997.933942844701</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.6 1202073.249 994.483542993679 2832096.261 1202083.235 994.483627463356 2832080.6 1202073.249 997.933942844701 2832080.6 1202073.249 994.483542993679</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.6 1202073.249 994.483542993679 2832094.859 1202085.435 994.483623206444 2832096.261 1202083.235 994.483627463356 2832080.6 1202073.249 994.483542993679</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.984 1202075.313 994.483537526234 2832094.859 1202085.435 994.483623206444 2832080.6 1202073.249 994.483542993679 2832078.984 1202075.313 994.483537526234</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
	<core:cityObjectMember>
		<bldg:Building gml:id="_2A0D50DD-B1DD-493C-B0DC-9DBDF70875A6">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20160105</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20151016</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Turm</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value>swisstopo</gen:value>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2014</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value>2015_Aufbau</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>2A0D50DD-B1DD-493C-B0DC-9DBDF70875A6</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.236 1202077.484 1004.04287344117 2832077.249 1202072.235 1006.10950346271 2832082.581 1202071.934 1004.04288845372 2832078.236 1202077.484 1004.04287344117</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832082.581 1202071.934 1004.04288845372 2832077.249 1202072.235 1006.10950346271 2832076.261 1202066.986 1004.04285250418 2832082.581 1202071.934 1004.04288845372</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832071.916 1202072.536 1004.04283738309 2832077.249 1202072.235 1006.10950346271 2832078.236 1202077.484 1004.04287344117 2832071.916 1202072.536 1004.04283738309</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832076.261 1202066.986 1004.04285250418 2832077.249 1202072.235 1006.10950346271 2832071.916 1202072.536 1004.04283738309 2832076.261 1202066.986 1004.04285250418</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832082.581 1202071.934 1004.04288845372 2832081.529 1202072.063 1004.42881638163 2832078.236 1202077.484 1004.04287344117 2832082.581 1202071.934 1004.04288845372</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832076.261 1202066.986 1004.04285250418 2832081.446 1202071.998 1004.4828065206 2832082.581 1202071.934 1004.04288845372 2832076.261 1202066.986 1004.04285250418</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832082.581 1202071.934 1004.04288845372 2832081.446 1202071.998 1004.4828065206 2832081.529 1202072.063 1004.42881638163 2832082.581 1202071.934 1004.04288845372</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.236 1202077.484 1004.04287344117 2832073.052 1202072.472 1004.48276630168 2832071.916 1202072.536 1004.04283738309 2832078.236 1202077.484 1004.04287344117</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832081.529 1202072.063 1004.42881638163 2832078.108 1202076.431 1004.42880454158 2832078.236 1202077.484 1004.04287344117 2832081.529 1202072.063 1004.42881638163</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 1004.42880454158 2832078.026 1202076.367 1004.48279468391 2832078.236 1202077.484 1004.04287344117 2832078.108 1202076.431 1004.42880454158</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.026 1202076.367 1004.48279468391 2832073.052 1202072.472 1004.48276630168 2832078.236 1202077.484 1004.04287344117 2832078.026 1202076.367 1004.48279468391</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832071.916 1202072.536 1004.04283738309 2832072.969 1202072.407 1004.42877521955 2832076.261 1202066.986 1004.04285250418 2832071.916 1202072.536 1004.04283738309</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832073.052 1202072.472 1004.48276630168 2832072.969 1202072.407 1004.42877521955 2832071.916 1202072.536 1004.04283738309 2832073.052 1202072.472 1004.48276630168</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832072.969 1202072.407 1004.42877521955 2832076.39 1202068.039 1004.42978695523 2832076.261 1202066.986 1004.04285250418 2832072.969 1202072.407 1004.42877521955</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832076.39 1202068.039 1004.42978695523 2832076.471 1202068.103 1004.48277820078 2832076.261 1202066.986 1004.04285250418 2832076.39 1202068.039 1004.42978695523</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832076.471 1202068.103 1004.48277820078 2832081.446 1202071.998 1004.4828065206 2832076.261 1202066.986 1004.04285250418 2832076.471 1202068.103 1004.48277820078</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.6 1202073.249 994.483542993679 2832081.529 1202072.063 1004.42881638163 2832081.529 1202072.063 986.2199834233 2832080.6 1202073.249 994.483542993679</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 1004.42880454158 2832081.529 1202072.063 1004.42881638163 2832079.757 1202074.325 998.877775792231 2832078.108 1202076.431 1004.42880454158</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832081.529 1202072.063 1004.42881638163 2832080.6 1202073.249 997.933942844701 2832079.757 1202074.325 998.877775792231 2832081.529 1202072.063 1004.42881638163</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.6 1202073.249 997.933942844701 2832081.529 1202072.063 1004.42881638163 2832080.6 1202073.249 994.483542993679 2832080.6 1202073.249 997.933942844701</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 986.219972065005 2832080.6 1202073.249 994.483542993679 2832081.529 1202072.063 986.2199834233 2832078.108 1202076.431 986.219972065005</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.984 1202075.313 994.483537526234 2832078.108 1202076.431 986.219972065005 2832078.108 1202076.431 1004.42880454158 2832078.984 1202075.313 994.483537526234</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832080.6 1202073.249 994.483542993679 2832078.108 1202076.431 986.219972065005 2832078.984 1202075.313 994.483537526234 2832080.6 1202073.249 994.483542993679</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832079.757 1202074.325 998.877775792231 2832079.029 1202075.254 998.009924267744 2832078.108 1202076.431 1004.42880454158 2832079.757 1202074.325 998.877775792231</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 1004.42880454158 2832078.984 1202075.313 997.95593350793 2832078.984 1202075.313 994.483537526234 2832078.108 1202076.431 1004.42880454158</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.984 1202075.313 997.95593350793 2832078.108 1202076.431 1004.42880454158 2832079.029 1202075.254 998.009924267744 2832078.984 1202075.313 997.95593350793</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 1004.42880454158 2832073.052 1202072.472 1004.48276630168 2832078.026 1202076.367 1004.48279468391 2832078.108 1202076.431 1004.42880454158</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 1004.42880454158 2832072.969 1202072.407 1004.42877521955 2832073.052 1202072.472 1004.48276630168 2832078.108 1202076.431 1004.42880454158</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 1004.42880454158 2832078.108 1202076.431 986.219972065005 2832072.969 1202072.407 1004.42877521955 2832078.108 1202076.431 1004.42880454158</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 986.219972065005 2832072.969 1202072.407 986.219943249436 2832072.969 1202072.407 1004.42877521955 2832078.108 1202076.431 986.219972065005</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832072.969 1202072.407 1004.42877521955 2832076.39 1202068.039 986.219954675925 2832076.39 1202068.039 1004.42978695523 2832072.969 1202072.407 1004.42877521955</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832072.969 1202072.407 986.219943249436 2832076.39 1202068.039 986.219954675925 2832072.969 1202072.407 1004.42877521955 2832072.969 1202072.407 986.219943249436</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832081.529 1202072.063 1004.42881638163 2832081.446 1202071.998 1004.4828065206 2832076.39 1202068.039 1004.42978695523 2832081.529 1202072.063 1004.42881638163</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832076.39 1202068.039 1004.42978695523 2832076.39 1202068.039 986.219954675925 2832081.529 1202072.063 1004.42881638163 2832076.39 1202068.039 1004.42978695523</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832081.529 1202072.063 1004.42881638163 2832076.39 1202068.039 986.219954675925 2832081.529 1202072.063 986.2199834233 2832081.529 1202072.063 1004.42881638163</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832076.39 1202068.039 1004.42978695523 2832081.446 1202071.998 1004.4828065206 2832076.471 1202068.103 1004.48277820078 2832076.39 1202068.039 1004.42978695523</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832081.529 1202072.063 986.2199834233 2832076.39 1202068.039 986.219954675925 2832078.108 1202076.431 986.219972065005 2832081.529 1202072.063 986.2199834233</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2832078.108 1202076.431 986.219972065005 2832076.39 1202068.039 986.219954675925 2832072.969 1202072.407 986.219943249436 2832078.108 1202076.431 986.219972065005</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
</core:CityModel>