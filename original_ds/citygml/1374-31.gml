<?xml version="1.0" encoding="UTF-8"?><!--CityGML-Datei erzeugt mit FME Desktop FME(R) 2016.1.3.0 , 10.06.2017 --><core:CityModel xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:bridge="http://www.opengis.net/citygml/bridge/2.0" xmlns:pbase="http://www.opengis.net/citygml/profiles/base/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0">
	<gml:name>swissBUILDINGS3D 2.0</gml:name>
	<gml:boundedBy>
		<gml:Envelope srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
			<gml:lowerCorner>2725017.264 1079685.241 828.77130550683</gml:lowerCorner>
			<gml:upperCorner>2725023.863 1079691.835 837.428742200371</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<core:cityObjectMember>
		<bldg:Building gml:id="_64B89FD3-1143-4EA3-AD34-9A645C4261B1">
			<gen:intAttribute name="DATUM_AENDERUNG">
				<gen:value>20160406</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="DATUM_ERSTELLUNG">
				<gen:value>20160406</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_JAHR">
				<gen:value>2016</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="ERSTELLUNG_MONAT">
				<gen:value>4</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="GRUND_AENDERUNG">
				<gen:value>Verbessert</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="HERKUNFT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:intAttribute name="HERKUNFT_JAHR">
				<gen:value>2015</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="HERKUNFT_MONAT">
				<gen:value>6</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="NAME_KOMPLETT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="OBJEKTART">
				<gen:value>Gebaeude Einzelhaus</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="ORIGINAL_HERKUNFT">
				<gen:value/>
			</gen:stringAttribute>
			<gen:intAttribute name="REVISION_JAHR">
				<gen:value>2016</gen:value>
			</gen:intAttribute>
			<gen:intAttribute name="REVISION_MONAT">
				<gen:value>4</gen:value>
			</gen:intAttribute>
			<gen:stringAttribute name="REVISION_QUALITAET">
				<gen:value/>
			</gen:stringAttribute>
			<gen:stringAttribute name="UUID">
				<gen:value>64B89FD3-1143-4EA3-AD34-9A645C4261B1</gen:value>
			</gen:stringAttribute>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725023.863 1079688.378 837.428741001138 2725017.264 1079688.699 835.879837330252 2725020.355 1079685.241 835.879836105853 2725023.863 1079688.378 837.428741001138</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725020.771 1079691.835 837.428742200371 2725017.264 1079688.699 835.879837330252 2725023.863 1079688.378 837.428741001138 2725020.771 1079691.835 837.428742200371</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725020.355 1079685.241 835.879836105853 2725020.395 1079685.948 836.044826107597 2725023.863 1079688.378 837.428741001138 2725020.355 1079685.241 835.879836105853</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725017.264 1079688.699 835.879837330252 2725017.97 1079688.659 836.043827129889 2725020.355 1079685.241 835.879836105853 2725017.264 1079688.699 835.879837330252</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725017.97 1079688.659 836.043827129889 2725020.395 1079685.948 836.044826107597 2725020.355 1079685.241 835.879836105853 2725017.97 1079688.659 836.043827129889</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725023.863 1079688.378 837.428741001138 2725023.156 1079688.418 837.26375126397 2725020.771 1079691.835 837.428742200371 2725023.863 1079688.378 837.428741001138</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725023.863 1079688.378 837.428741001138 2725020.395 1079685.948 836.044826107597 2725023.156 1079688.418 837.26375126397 2725023.863 1079688.378 837.428741001138</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725020.771 1079691.835 837.428742200371 2725020.731 1079691.129 837.263752206118 2725017.264 1079688.699 835.879837330252 2725020.771 1079691.835 837.428742200371</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725023.156 1079688.418 837.26375126397 2725020.731 1079691.129 837.263752206118 2725020.771 1079691.835 837.428742200371 2725023.156 1079688.418 837.26375126397</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725020.731 1079691.129 837.263752206118 2725017.97 1079688.659 836.043827129889 2725017.264 1079688.699 835.879837330252 2725020.731 1079691.129 837.263752206118</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725023.156 1079688.418 837.26375126397 2725023.156 1079688.418 828.771310962689 2725020.731 1079691.129 837.263752206118 2725023.156 1079688.418 837.26375126397</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725023.156 1079688.418 828.771310962689 2725020.731 1079691.129 828.771311926537 2725020.731 1079691.129 837.263752206118 2725023.156 1079688.418 828.771310962689</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725020.731 1079691.129 837.263752206118 2725020.731 1079691.129 828.771311926537 2725017.97 1079688.659 836.043827129889 2725020.731 1079691.129 837.263752206118</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725020.731 1079691.129 828.771311926537 2725017.97 1079688.659 828.771306481744 2725017.97 1079688.659 836.043827129889 2725020.731 1079691.129 828.771311926537</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725017.97 1079688.659 836.043827129889 2725020.395 1079685.948 828.77130550683 2725020.395 1079685.948 836.044826107597 2725017.97 1079688.659 836.043827129889</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725017.97 1079688.659 828.771306481744 2725020.395 1079685.948 828.77130550683 2725017.97 1079688.659 836.043827129889 2725017.97 1079688.659 828.771306481744</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725020.395 1079685.948 836.044826107597 2725023.156 1079688.418 828.771310962689 2725023.156 1079688.418 837.26375126397 2725020.395 1079685.948 836.044826107597</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725020.395 1079685.948 836.044826107597 2725020.395 1079685.948 828.77130550683 2725023.156 1079688.418 828.771310962689 2725020.395 1079685.948 836.044826107597</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="urn:ogc:def:crs:EPSG:2056, crs:EPSG:5728" srsDimension="3">
							<gml:surfaceMember>
								<gml:CompositeSurface>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725023.156 1079688.418 828.771310962689 2725017.97 1079688.659 828.771306481744 2725020.731 1079691.129 828.771311926537 2725023.156 1079688.418 828.771310962689</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
									<gml:surfaceMember>
										<gml:Polygon>
											<gml:exterior>
												<gml:LinearRing>
													<gml:posList>2725023.156 1079688.418 828.771310962689 2725020.395 1079685.948 828.77130550683 2725017.97 1079688.659 828.771306481744 2725023.156 1079688.418 828.771310962689</gml:posList>
												</gml:LinearRing>
											</gml:exterior>
										</gml:Polygon>
									</gml:surfaceMember>
								</gml:CompositeSurface>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
</core:CityModel>