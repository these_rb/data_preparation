# Préparation des données

### Original_ds
Contient les fichiers originaux, transformés à chaque étape.

### 01. DB preparation
Contient les différentes étapes à réaliser pour télécharger des données OSM et les stocker sous la forme voulue dans une BDD postgis ainsi que l'import des données RegBl de catégorie A.

### 02. citygml_to_json
Contient le script nécessaire à transformer les fichiers de cityGML à cityJSON.
Ce script nécessite les [citygml-tools](https://github.com/citygml4j/citygml-tools/releases)

### 03. Convert_json _to_hdf5

Contient le script qui permet de passer d'un citymodel à un ensemble de rasters/images des bâtiments. 

Actuellement, le script permet de possiblement créer les éléments suivants:

- récupérer l'id (egid), la catégorie et la classe du bâtiment
- un array de la hauteur (différence sol-toit) d'un bâtiment
- la couverture autour d'un bâtiment avec une résolution définie (une résolution de 1m couvrira 112m de chaque côté du centre du bâtiment, une résolution de 2m, 224m de chaque côté, etc.)

Ce qu'il faut encore faire:

- Ajuster le module des indices de shapes
- Finir le script pour avoir un enregistrement dans des hdf5 différents
- Éventuellement faire qu'on puisse sélectionner quels hdf5 sont créés

### 04. Post_processing



