import os
import subprocess
from datetime import datetime
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input_path", required=True, help="Input path string")
ap.add_argument("-o", "--output_path", required=True, help="Output path string")
ap.add_argument("-c", "--city_tools_path", required=True, help="City tools path string")
ap.add_argument("-st", "--start_pct", type=float, default=0.0,
                help="What pct of input to start with")
ap.add_argument("-sp", "--stop_pct", type=float, default=1.0,
                help="What pct of input to end with")
ap.add_argument("-u", "--update_freq", type=int, default=25,
                help="Update frequences (nb. of files)")
args = ap.parse_args()


def gml2json(input_path, output_path, city_tools_path, start=0, stop=1, progUpdate=25):
    """"
    This function takes an input directory path, output directory path and path
    to the citygml utility tools. An opt int to have a view on the progress
    https://github.com/citygml4j/citygml-tools/releases

    It converts all .gml in the input dir to .json files in the output dir
    using the command line utility. If an error occurs during the conversion
    the file name is printed for further investigation.

    Start and stop can be used to convert only a fraction of the files

    At the end a summary of the number of converted files is printed
    """
    # Check if inputs are of the correct type
    if not os.path.isdir(input_path) or not os.path.isdir(output_path):
        raise NotADirectoryError("Input et/ou Output must be folders")
    elif not os.path.isfile(city_tools_path):
        raise FileExistsError("citygml-tools must be included at end of path")

    files_to_convert = []
    files_input_path = os.listdir(input_path)

    # Keep only .gml files
    for fname in files_input_path:
        if fname.endswith(".gml"):
            files_to_convert.append(fname)
    # Get only the chosen percentage of files
    files_to_convert = sorted(files_to_convert)
    files_to_convert = files_to_convert[int(len(files_to_convert) * start) : int(len(files_to_convert) * stop)]

    # Counters for the summary
    all_files = len(files_to_convert)
    converted_files = all_files
    actual_file = 0

    for fname in files_to_convert:
        actual_file += 1
        if actual_file % progUpdate == 0:
            print("{} | Converting file {}, n°{} on {}...".format(datetime.now(), fname, actual_file, all_files))

        convert_cmd = " ".join([city_tools_path, "to-cityjson", "--epsg=2056"])
        jsonfname = fname.replace(".gml", ".json")
        # Adjust paths
        path = os.path.join(input_path, fname)
        src_path_json = os.path.join(input_path, jsonfname)
        dest_path_json = os.path.join(output_path, jsonfname)
        # Adjust command
        convert_cmd += " " + path
        # Execute process and check for error
        if subprocess.call(convert_cmd, shell=True, stdout=open(os.devnull, 'wb')):
            print("File {} caused an error".format(fname))
            converted_files -= 1
            continue
        # Write file to output path
        os.replace(src_path_json, dest_path_json)

    # Print the summary
    if all_files == converted_files:
        print('\033[1m' + "Successfully converted all {} files".format(all_files))
    else:
        print('\033[1m' + "Converted {} files on {}".format(converted_files, all_files))


gml2json(args.input_path, args.output_path, args.city_tools_path,
         args.start_pct, args.stop_pct, args.update_freq)
